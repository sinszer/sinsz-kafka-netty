package com.sinsz.im.repository.support;

/**
 * @author chenjianbo
 * @date 2017/9/29
 */
public interface RedisConstant {

    /**
     * CM机器群的注册信息
     */
    String REG = "{cm.reg}.connector";

    /**
     * 接收监听失败次数记录
     */
    String RECEIVE_FAIL = "{cm.receive.fail}.";

    /**
     * 接收监听成功的消息记录
     * 存7天的所有记录
     */
    String RECEIVE_SUCCESS = "{cm.receive.success}.";

    /**
     * 用户登录键前缀
     */
    String TOKEN = "{cm.token}.";

    /**
     * 用户所有登录键前缀
     */
    String MULTI_TOKEN = "{cm.multi.token}.";

    /**
     * 用户Sequence前缀
     */
    String USER_SEQUENCE = "{cm.sequence}.";

    /**
     * 全局的消息编号
     */
    String MESSAGE_ID = "{cm.message.id}.messageId";

    /**
     * 会话ID前缀
     */
    String SESSION_ID = "{cm.session.id}.";

    /**
     * 用户群组
     */
    String GROUP_INFO = "{cm.group.info.id}.";

    /**
     * 用户未读消息前缀
     */
    String USER_UNREAD = "{cm.user.unread}.";

}

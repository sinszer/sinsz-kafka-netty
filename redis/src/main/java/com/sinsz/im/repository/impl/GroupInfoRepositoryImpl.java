package com.sinsz.im.repository.impl;

import com.sinsz.im.repository.GroupInfoRepository;
import com.sinsz.im.repository.support.GroupInfo;
import com.sinsz.im.repository.support.RedisConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 群组信息
 * @author chenjianbo
 * @date 2017/10/13
 */
@Repository("groupInfoRepository")
public class GroupInfoRepositoryImpl implements GroupInfoRepository {

    @Autowired
    @Qualifier("objRedisTemplate")
    private RedisTemplate<String,Object> redisTemplate;

    private String key(String id) {
        return RedisConstant.GROUP_INFO + id;
    }

    @Override
    public boolean exist(String gId) {
        return redisTemplate.hasKey(key(gId));
    }
    @Override
    public boolean exist(String gId, GroupInfo groupInfo) {
        return redisTemplate.opsForSet().isMember(key(gId), groupInfo);
    }
    @Override
    public void delete(String gId) {
        redisTemplate.delete(key(gId));
    }
    @Override
    public void delete(String gId, GroupInfo groupInfo) {
        redisTemplate.opsForSet().remove(key(gId), groupInfo);
    }
    @Override
    public void insert(String gId, GroupInfo groupInfo) {
        redisTemplate.opsForSet().add(key(gId), groupInfo);
    }
    @Override
    public List<GroupInfo> getAll(String gId) {
        return redisTemplate.opsForSet().members(key(gId))
                .stream().map(obj -> (GroupInfo)obj).collect(Collectors.toList());
    }

}
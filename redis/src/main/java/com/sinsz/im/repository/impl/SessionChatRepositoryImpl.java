package com.sinsz.im.repository.impl;

import com.sinsz.im.repository.SessionChatRepository;
import com.sinsz.im.repository.support.RedisConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 会话
 * 用于单聊、群聊的处理
 * @author chenjianbo
 * @date 2017/10/13
 */
@Repository("sessionChatRepository")
public class SessionChatRepositoryImpl implements SessionChatRepository {

    @Autowired
    @Qualifier("objRedisTemplate")
    private RedisTemplate<String,Object> redisTemplate;

    private String key(String from, String to) {
        from = StringUtils.isEmpty(from)?"":from;
        to = StringUtils.isEmpty(to)?"":to;
        return RedisConstant.SESSION_ID + from + "@" + to;
    }

    private boolean isExist(String from, String to) {
        return redisTemplate.hasKey(key(from, to));
    }

    /**
     * 根据当前存在情况获取对应键
     * @param from
     * @param to
     * @param exist
     * @return
     */
    private String K(String from, String to, int exist) {
        if (exist == 2) {
            return key(to, from);
        }
        return key(from, to);
    }

    /**
     * 对外验证是否存在会话ID
     * @param from
     * @param to
     * @return
     *      0: 表示不存在；
     *      1：表示from-to存在；
     *      2：表示to-from存在；
     */
    @Override
    public int exist(String from, String to) {
        if (isExist(from, to)) {
            return 1;
        }
        if (isExist(to, from)) {
            return 2;
        }
        return 0;
    }
    @Override
    public void insert(String from, String to, int exist, String userId) {
        redisTemplate.opsForSet().add(K(from, to, exist), userId);
    }
    @Override
    public void delete(String from, String to, int exist, String userId) {
        redisTemplate.opsForSet().remove(K(from, to, exist), userId);
    }
    @Override
    public List<String> getAll(String from,String to, int exist) {
        return redisTemplate.opsForSet().members(K(from, to, exist))
                .stream()
                .map(Object::toString)
                .collect(Collectors.toList());
    }
    @Override
    public void delete(String from, String to, int exist) {
        redisTemplate.delete(K(from, to, exist));
    }

}
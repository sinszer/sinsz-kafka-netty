package com.sinsz.im.repository;

import com.sinsz.im.repository.support.GroupInfo;

import java.util.List;

/**
 * 群组信息
 * @author chenjianbo
 * @date 2017/10/13
 */
public interface GroupInfoRepository {

    boolean exist(String gId) ;

    boolean exist(String gId, GroupInfo groupInfo) ;

    void delete(String gId) ;

    void delete(String gId, GroupInfo groupInfo) ;

    void insert(String gId, GroupInfo groupInfo) ;

    List<GroupInfo> getAll(String gId);
}

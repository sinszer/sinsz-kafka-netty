package com.sinsz.im.repository;

import com.sinsz.im.protocol.SMessage;

import java.util.Set;

/**
 * 存储7天的聊天或发送命令成功的消息记录
 *
 *  ZSet
 * <b>
 *      {cm.receive.success}.token_userId
 * </b>
 *
 * @author chenjianbo
 * @date 2017/9/30
 */
public interface KafkaReceiveSuccessRepository {

    void insert(String token, String userId, SMessage.MessageBuf messageBuf);

    void delete(String token, String userId, SMessage.MessageBuf messageBuf);

    Set<Object> getAll(String token, String userId);

}

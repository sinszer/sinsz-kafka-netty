package com.sinsz.im.repository.impl;

import com.sinsz.im.repository.KafkaReceiveFailRepository;
import com.sinsz.im.repository.support.RedisConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

/**
 * 消息队列接收失败的次数记录
 * @author chenjianbo
 * @date 2017/9/30
 */
@Repository("kafkaReceiveFailRepository")
public class KafkaReceiveFailRepositoryImpl implements KafkaReceiveFailRepository {

    @Autowired
    @Qualifier("objRedisTemplate")
    private RedisTemplate<String, Object> redisTemplate;

    private String key(String kafkaKey) {
        return RedisConstant.RECEIVE_FAIL + kafkaKey;
    }

    @Override
    public boolean exist(String kafkaKey) {
        Assert.hasText(kafkaKey, "请输入键值");
        return redisTemplate.hasKey(key(kafkaKey));
    }

    @Override
    public void delete(String kafkaKey) {
        if (exist(kafkaKey)) {
            redisTemplate.delete(key(kafkaKey));
        }
    }

    @Override
    public int get(String kafkaKey) {
        if (exist(kafkaKey)) {
            return Integer.valueOf(
                    redisTemplate.opsForValue().get(key(kafkaKey)).toString()
            );
        }
        return 0;
    }

    @Override
    public void insert(String kafkaKey) {
        int i = get(kafkaKey) + 1;
        redisTemplate.opsForValue().set(key(kafkaKey), i);
    }

    @Override
    public void update(String kafkaKey) {
        insert(kafkaKey);
    }
}
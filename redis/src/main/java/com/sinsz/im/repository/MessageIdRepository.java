package com.sinsz.im.repository;

/**
 * 全局消息编号ID
 * @author chenjianbo
 * @date 2017/10/12
 */
public interface MessageIdRepository {

    long get();

}
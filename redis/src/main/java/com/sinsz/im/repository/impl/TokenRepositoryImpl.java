package com.sinsz.im.repository.impl;

import com.sinsz.im.repository.TokenRepository;
import com.sinsz.im.repository.support.RedisConstant;
import com.sinsz.im.repository.support.Token;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;
import org.springframework.util.ObjectUtils;

import java.util.concurrent.TimeUnit;

@Repository("tokenRepository")
public class TokenRepositoryImpl implements TokenRepository {
    @Autowired
    @Qualifier("objRedisTemplate")
    private RedisTemplate<String, Object> redisTemplate;

    private String key(String uuid) {
        return RedisConstant.TOKEN + uuid;
    }

    @Override
    public boolean exist(String uuid) {
        Assert.hasText(uuid, "用户登录键不能为空");
        return redisTemplate.hasKey(key(uuid));
    }

    @Override
    public void delete(String uuid) {
        if (exist(uuid)) {
            redisTemplate.delete(key(uuid));
        }
    }

    @Override
    public void insert(String uuid, Token token) {
        Assert.hasText(uuid, "用户登录键不能为空");
        Assert.isTrue(!ObjectUtils.isEmpty(token), "登录信息不能为空");
        redisTemplate.opsForValue().set(key(uuid), token, 7, TimeUnit.DAYS);
    }

    @Override
    public Token get(String uuid) {
        if (exist(uuid)) {
            Object obj = redisTemplate.opsForValue().get(key(uuid));
            return obj != null ? (Token) obj: null;
        }
        return null;
    }
}

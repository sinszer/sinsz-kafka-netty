package com.sinsz.im.repository.impl;

import com.sinsz.im.repository.SequenceRepository;
import com.sinsz.im.repository.support.RedisConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

@Repository("sequenceRepository")
public class SequenceRepositoryImpl implements SequenceRepository {

    @Autowired
    @Qualifier("strRedisTemplate")
    private StringRedisTemplate redisTemplate;

    private String key(String userId) {
        return RedisConstant.USER_SEQUENCE + userId;
    }

    @Override
    public boolean exist(String userId) {
        Assert.hasText(userId, "用户ID不能为空");
        return redisTemplate.hasKey(key(userId));
    }

    @Override
    public void insert(String userId, long sequence) {
        Assert.hasText(userId, "用户ID不能为空");
        Assert.isTrue(sequence >= 0, "非法的序列编号");
        if (!exist(userId)) {
            redisTemplate.opsForValue().set(key(userId), String.valueOf(sequence));
            return;
        }
        Assert.isTrue(sequence >= get(userId), "不是最新的消息序列编号");
        redisTemplate.opsForValue().set(key(userId), String.valueOf(sequence));
    }

    @Override
    public long get(String userId) {
        if (exist(userId)) {
            return Long.valueOf(
                    redisTemplate.opsForValue().get(key(userId))
            );
        }
        return 0L;
    }
}
package com.sinsz.im.repository;

import com.sinsz.im.repository.support.UnRead;

import java.util.List;

/**
 * 未读消息
 * @author chenjianbo
 * @date 2017/10/16
 */
public interface UnReadRepository {

    boolean exist(String userId);

    void delete(String userId);

    void delete(String userId, UnRead unRead);

    void insert(String userId, UnRead unRead);

    List<UnRead> getAll(String userId);
}

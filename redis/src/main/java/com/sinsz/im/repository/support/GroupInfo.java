package com.sinsz.im.repository.support;

import com.sinsz.im.enums.IdentifyEnum;

import java.io.Serializable;

/**
 * redis中群成员信息
 * @author chenjianbo
 * @date 2017/10/13
 */
public class GroupInfo extends Object implements Serializable {

    private static final long serialVersionUID = -3334424868702233506L;

    /**
     * 用户成员
     */
    private String userId;

    /**
     * 身份
     */
    private IdentifyEnum identify;

    public GroupInfo() {
    }

    public GroupInfo(String userId, IdentifyEnum identify) {
        this.userId = userId;
        this.identify = identify;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public IdentifyEnum getIdentify() {
        return identify;
    }

    public void setIdentify(IdentifyEnum identify) {
        this.identify = identify;
    }
}

package com.sinsz.im.repository;

import java.util.Map;

/**
 * CM注册信息
 *
 *  Hash
 * <b>
 *     {cm.reg}.connector
 * </b>
 *
 * @author chenjianbo
 * @date 2017/9/29
 */
public interface RegConnectorRepository {

    void insert(String ip, int port, String topic);

    void update(String ip, int port, String topic);

    void delete(String ip, int port);

    boolean exist(String ip, int port);

    String get(String ip, int port);

    Map<Object, Object> getAll();

    void delete();

    boolean exist();

}
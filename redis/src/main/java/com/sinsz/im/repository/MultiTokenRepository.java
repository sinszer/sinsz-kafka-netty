package com.sinsz.im.repository;

import java.util.Set;

/**
 * 用户登录的所有token集合
 * @author chenjianbo
 * @date 2017/10/10
 */
public interface MultiTokenRepository {

    boolean exist(String userId, String uuid);

    boolean exist(String userId);

    void delete(String userId, String uuid);

    void delete(String userId);

    void insert(String userId, String uuid);

    Set<Object> getAll(String userId);

}

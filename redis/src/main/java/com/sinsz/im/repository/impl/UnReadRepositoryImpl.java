package com.sinsz.im.repository.impl;

import com.sinsz.im.repository.UnReadRepository;
import com.sinsz.im.repository.support.RedisConstant;
import com.sinsz.im.repository.support.UnRead;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 未读消息
 * @author chenjianbo
 * @date 2017/10/16
 */
@Repository("unReadRepository")
public class UnReadRepositoryImpl implements UnReadRepository {

    @Autowired
    @Qualifier("objRedisTemplate")
    private RedisTemplate<String, Object> redisTemplate;

    private String key(String userId) {
        return RedisConstant.USER_UNREAD + userId;
    }

    @Override
    public boolean exist(String userId) {
        Assert.hasText(userId, "用户ID不能为空");
        return redisTemplate.hasKey(key(userId));
    }

    @Override
    public void delete(String userId) {
        if (exist(userId)) {
            redisTemplate.delete(key(userId));
        }
    }

    @Override
    public void delete(String userId, UnRead unRead) {
        if (exist(userId)) {
            redisTemplate.opsForZSet().remove(key(userId), unRead);
        }
    }

    @Override
    public void insert(String userId, UnRead unRead) {
        Assert.hasText(userId, "用户ID不能为空");
        Assert.notNull(unRead, "未读消息不能为空");
        redisTemplate.opsForZSet().add(key(userId), unRead, System.nanoTime());
    }

    @Override
    public List<UnRead> getAll(String userId) {
        if (exist(userId)) {
            return redisTemplate.opsForZSet().range(key(userId),0,-1)
                    .stream().map(obj -> (UnRead)obj).collect(Collectors.toList());
        }
        return null;
    }

}
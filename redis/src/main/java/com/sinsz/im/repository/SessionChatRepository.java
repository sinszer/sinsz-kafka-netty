package com.sinsz.im.repository;

import java.util.List;

/**
 * 会话
 * <p>
 *     单聊：fromUserId->toUserId为键存储所有的人
 *     群聊：toUserId为键存储所有的人，每次信息上来去检测群成员与当前会话成员的最新数据
 * </p>
 * @author chenjianbo
 * @date 2017/10/13
 */
public interface SessionChatRepository {
    /**
     * 对外验证是否存在会话ID
     * @param from
     * @param to
     * @return
     *      0: 表示不存在；
     *      1：表示from-to存在；
     *      2：表示to-from存在；
     */
    int exist(String from, String to);

    /**
     * 插入成员
     * @param from
     * @param to
     * @param exist
     * @param userId
     */
    void insert(String from, String to, int exist, String userId);

    /**
     * 删除成员
     * @param from
     * @param to
     * @param exist
     * @param userId
     */
    void delete(String from, String to, int exist, String userId);

    /**
     * 获取全部成员
     * @param from
     * @param to
     * @param exist
     * @return
     */
    List<String> getAll(String from, String to, int exist);

    /**
     * 删除键
     * @param from
     * @param to
     * @param exist
     */
    void delete(String from, String to, int exist);
}
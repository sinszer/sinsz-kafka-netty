package com.sinsz.im.repository;

/**
 * 消息队列接收失败的次数记录
 *
 * String
 * <b>
 *     键为发送到队列中去的key(UUID):
 *     {cm.receive.fail}.key
 * </b>
 *
 * @author chenjianbo
 * @date 2017/9/30
 */
public interface KafkaReceiveFailRepository {

    boolean exist(String kafkaKey);

    void delete(String kafkaKey);

    int get(String kafkaKey);

    void insert(String kafkaKey);

    void update(String kafkaKey);

}
package com.sinsz.im.repository.impl;

import com.sinsz.im.repository.MessageIdRepository;
import com.sinsz.im.repository.support.RedisConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.support.atomic.RedisAtomicLong;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

@Repository("messageIdRepository")
public class MessageIdRepositoryImpl implements MessageIdRepository {

    @Autowired
    @Qualifier("strRedisTemplate")
    private StringRedisTemplate redisTemplate;

    @Override
    public long get() {
        RedisAtomicLong entityIdCounter = new RedisAtomicLong(
                RedisConstant.MESSAGE_ID,
                redisTemplate.getConnectionFactory()
        );
        Long increment = entityIdCounter.incrementAndGet();
        Assert.isTrue(increment > 0, "全局消息编号异常");
        return increment;
    }
}

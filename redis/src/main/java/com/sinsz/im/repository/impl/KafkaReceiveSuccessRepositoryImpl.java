package com.sinsz.im.repository.impl;

import com.sinsz.im.protocol.SMessage;
import com.sinsz.im.repository.KafkaReceiveSuccessRepository;
import com.sinsz.im.repository.support.RedisConstant;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * 存储7天的聊天或发送命令成功的消息记录
 * @author chenjianbo
 * @date 2017/9/30
 */
@Repository("kafkaReceiveSuccessRepository")
public class KafkaReceiveSuccessRepositoryImpl implements KafkaReceiveSuccessRepository {

    @Autowired
    @Qualifier("objRedisTemplate")
    private RedisTemplate<String, Object> redisTemplate;

    private String key(String token, String userId) {
        return RedisConstant.RECEIVE_SUCCESS + token + "_" + userId;
    }

    private boolean exist(String token, String userId) {
        return redisTemplate.hasKey(key(token, userId));
    }

    @Override
    public void insert(String token, String userId, SMessage.MessageBuf messageBuf) {
        token = StringUtils.isEmpty(token)?"":token;
        userId = StringUtils.isEmpty(userId)?"":userId;
        redisTemplate.opsForZSet().add(
                key(token, userId),
                messageBuf,
                Double.valueOf(String.valueOf(System.currentTimeMillis()))
        );
        redisTemplate.expire(key(token, userId), 3, TimeUnit.DAYS);
    }

    @Override
    public void delete(String token, String userId, SMessage.MessageBuf messageBuf) {
        token = StringUtils.isEmpty(token)?"":token;
        userId = StringUtils.isEmpty(userId)?"":userId;
        if (exist(token, userId)) {
            redisTemplate.opsForZSet().remove(key(token, userId), messageBuf);
        }
    }

    @Override
    public Set<Object> getAll(String token, String userId) {
        token = StringUtils.isEmpty(token)?"":token;
        userId = StringUtils.isEmpty(userId)?"":userId;
        if (exist(token, userId)) {
            return redisTemplate.opsForZSet().range(key(token, userId),0,-1);
        }
        return null;
    }
}

package com.sinsz.im.repository.support;

import com.sinsz.im.protocol.SMessage;

import java.io.Serializable;

/**
 * 未读消息集
 * @author chenjianbo
 * @date 2017/10/16
 */
public class UnRead implements Serializable {

    private static final long serialVersionUID = 6910264116594287463L;

    public UnRead() {
    }

    public UnRead(SMessage.MessageBuf messageBuf) {
        this.messageBuf = messageBuf;
    }

    private SMessage.MessageBuf messageBuf;

    public SMessage.MessageBuf getMessageBuf() {
        return messageBuf;
    }

    public void setMessageBuf(SMessage.MessageBuf messageBuf) {
        this.messageBuf = messageBuf;
    }
}

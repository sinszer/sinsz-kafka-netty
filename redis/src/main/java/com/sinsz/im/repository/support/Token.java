package com.sinsz.im.repository.support;

import com.sinsz.im.enums.AppEnum;

import java.io.Serializable;

public class Token implements Serializable {

    private static final long serialVersionUID = -6636035070323965204L;
    // 用户ID
    private String userId;
    // 用户姓名
    private String name;
    // 登录时间
    private long loginTime;
    // 推送ID
    private String deviceToken;
    // 应用类型
    private AppEnum app;
    // 消息队列编码
    private String topic;
    // 通道ID
    private String channelId;

    public Token() {
    }

    public Token(String userId, String name, long loginTime, String deviceToken, AppEnum app) {
        this.userId = userId;
        this.name = name;
        this.loginTime = loginTime;
        this.deviceToken = deviceToken;
        this.app = app;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getLoginTime() {
        return loginTime;
    }

    public void setLoginTime(long loginTime) {
        this.loginTime = loginTime;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public AppEnum getApp() {
        return app;
    }

    public void setApp(AppEnum app) {
        this.app = app;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }
}

package com.sinsz.im.repository.impl;

import com.sinsz.im.repository.MultiTokenRepository;
import com.sinsz.im.repository.support.RedisConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import java.util.Set;

@Repository("multiTokenRepository")
public class MultiTokenRepositoryImpl implements MultiTokenRepository {

    @Autowired
    @Qualifier("objRedisTemplate")
    private RedisTemplate<String, Object> redisTemplate;

    private String key(String userId) {
        return RedisConstant.MULTI_TOKEN + userId;
    }

    @Override
    public boolean exist(String userId, String uuid) {
        Assert.hasText(userId, "用户ID不能为空");
        Assert.hasText(uuid, "用户登录唯一标识不能为空");
        return redisTemplate.opsForSet().isMember(key(userId), uuid);
    }

    @Override
    public boolean exist(String userId) {
        Assert.hasText(userId, "用户ID不能为空");
        return redisTemplate.hasKey(key(userId));
    }

    @Override
    public void delete(String userId, String uuid) {
        if (exist(userId, uuid)) {
            redisTemplate.opsForSet().remove(key(userId), uuid);
        }
    }

    @Override
    public void delete(String userId) {
        if (exist(userId)) {
            redisTemplate.delete(key(userId));
        }
    }

    @Override
    public void insert(String userId, String uuid) {
        Assert.hasText(userId, "用户ID不能为空");
        Assert.hasText(uuid, "用户登录唯一标识不能为空");
        delete(userId, uuid);
        redisTemplate.opsForSet().add(key(userId), uuid);
    }

    @Override
    public Set<Object> getAll(String userId) {
        if (exist(userId)) {
            return redisTemplate.opsForSet().members(key(userId));
        }
        return null;
    }
}

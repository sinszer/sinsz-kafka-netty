package com.sinsz.im.repository.impl;

import com.sinsz.im.repository.RegConnectorRepository;
import com.sinsz.im.repository.support.RedisConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import java.util.Map;

/**
 * CM注册信息
 * @author chenjianbo
 * @date 2017/9/29
 */
@Repository("regConnectorRepository")
public class RegConnectorRepositoryImpl implements RegConnectorRepository {

    @Autowired
    @Qualifier("objRedisTemplate")
    private RedisTemplate<String, Object> redisTemplate;

    private static final String IP_NULL = "IP未输入";

    private static final String TOPIC_NULL = "TOPIC主题未输入";

    private static final String PORT_NULL = "端口号不存在";

    @Override
    public boolean exist() {
        return redisTemplate.hasKey(RedisConstant.REG);
    }

    @Override
    public void delete() {
        if (exist()) {
            redisTemplate.delete(RedisConstant.REG);
        }
    }

    @Override
    public boolean exist(String ip, int port) {
        Assert.hasText(ip, IP_NULL);
        Assert.isTrue(port > 0, PORT_NULL);
        if (exist()) {
            return redisTemplate.opsForHash().hasKey(
                    RedisConstant.REG,
                    ip + ":" + String.valueOf(port)
            );
        }
        return false;
    }

    @Override
    public void delete(String ip, int port) {
        Assert.hasText(ip, IP_NULL);
        Assert.isTrue(port > 0, PORT_NULL);
        if (exist(ip, port)) {
            redisTemplate.opsForHash().delete(
                    RedisConstant.REG,
                    ip + ":" + String.valueOf(port)
            );
        }
    }

    @Override
    public void insert(String ip, int port, String topic) {
        Assert.hasText(ip, IP_NULL);
        Assert.isTrue(port > 0, PORT_NULL);
        Assert.hasText(topic, TOPIC_NULL);
        redisTemplate.opsForHash().put(
                RedisConstant.REG,
                ip + ":" + String.valueOf(port),
                topic
        );
    }

    @Override
    public void update(String ip, int port, String topic) {
        delete(ip, port);
        insert(ip, port, topic);
    }

    @Override
    public String get(String ip, int port) {
        Assert.hasText(ip, IP_NULL);
        Assert.isTrue(port > 0, PORT_NULL);
        if (exist(ip, port)) {
            return (String) redisTemplate.opsForHash().get(
                    RedisConstant.REG,
                    ip + ":" + String.valueOf(port)
            );
        }
        return null;
    }

    @Override
    public Map<Object, Object> getAll() {
        if (exist()) {
            return redisTemplate.opsForHash().entries(RedisConstant.REG);
        }
        return null;
    }

}
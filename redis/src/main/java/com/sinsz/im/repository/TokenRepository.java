package com.sinsz.im.repository;

import com.sinsz.im.repository.support.Token;

/**
 * Token管理
 * <b>
 *     默认7天登录有效期
 * </b>
 * @author chenjianbo
 * @date 2017/10/10
 */
public interface TokenRepository {

    boolean exist(String uuid);

    void delete(String uuid);

    void insert(String uuid, Token token);

    Token get(String uuid);
}

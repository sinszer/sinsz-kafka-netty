package com.sinsz.im.repository;

/**
 * 单用户局部消息编号
 * @author chenjianbo
 * @date 2017/10/11
 */
public interface SequenceRepository {

    boolean exist(String userId);

    void insert(String userId, long sequence);

    long get(String userId);

}

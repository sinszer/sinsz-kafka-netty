package com.sinsz.im.configuration;

import com.sinsz.im.configuration.redis.ObjectRedisSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.serializer.StringRedisSerializer;

@Configuration
@Order(2)
public class RedisSerializeConfiguration {

    private static final Logger logger = LoggerFactory.getLogger(RedisSerializeConfiguration.class);

    public RedisSerializeConfiguration() {
        logger.info(">>>>>>>>>>>>>>>>>>>> Redis序列初始化 >>>>>>>>>>>>>>>>>>");
    }

    @Autowired(required = false)
    private JedisConnectionFactory jedisConnectionFactory;

    /**
     * 对象序列化
     */
    @Bean
    public RedisTemplate<String, Object> objRedisTemplate() {
        RedisTemplate<String, Object> template = new RedisTemplate<>();
        template.setConnectionFactory(jedisConnectionFactory);
        template.setKeySerializer(new StringRedisSerializer());
        template.setValueSerializer(new ObjectRedisSerializer());
        template.setDefaultSerializer(new StringRedisSerializer());
        return template;
    }

    /**
     * 字符串序列化
     */
    @Bean
    public StringRedisTemplate strRedisTemplate() {
        StringRedisTemplate template = new StringRedisTemplate();
        template.setConnectionFactory(jedisConnectionFactory);
        template.setKeySerializer(new StringRedisSerializer());
        template.setValueSerializer(new StringRedisSerializer());
        return template;
    }

}
package com.sinsz.im.configuration;

import com.sinsz.im.common.SpringContextHolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.annotation.Order;
import org.springframework.scheduling.annotation.EnableAsync;

@Configuration
@EnableAsync                            //开启异步注解
@ComponentScan({"com.sinsz.im"})        //扫描所有对象
@Import({SpringContextHolder.class})
@Order(1)
public class CommonlyConfiguration {
    private static final Logger logger = LoggerFactory.getLogger(CommonlyConfiguration.class);

    public CommonlyConfiguration() {
        logger.info(">>>>>>>>>>>>>>>>>>>> 全局配置 >>>>>>>>>>>>>>>>>>");
    }

}
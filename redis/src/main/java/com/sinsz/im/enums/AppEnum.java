package com.sinsz.im.enums;

/**
 * 应用类型
 * @author chenjianbo
 * @date 2017/10/10
 */
public enum  AppEnum {

    IOS,
    ANDROID,
    PC

}
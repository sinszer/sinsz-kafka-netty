package com.sinsz.im.enums;

/**
 * 群成员身份
 * @author chenjianbo
 * @date 2017/10/13
 */
public enum IdentifyEnum {

    /**
     * 分别表示：
     * 群主；
     * 管理员；
     * 成员
     */
    LEADER,

    ADMIN,

    MEMBER

}

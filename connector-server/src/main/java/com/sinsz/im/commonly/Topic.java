package com.sinsz.im.commonly;

import com.sinsz.im.kafka.KafkaTopic;

/**
 * 初始化Topic
 * @author chenjianbo
 * @date 2017/9/28
 */
public class Topic {

    private Topic() {
    }

    private static class TopicImpl {
        private static Topic topic = new Topic();
    }

    public static Topic getInstance() {
        return TopicImpl.topic;
    }

    private static String topic;

    public void initTopic() {
        topic = String.valueOf(System.nanoTime());
    }

    public String getTopic() {
        return KafkaTopic.SEND_TOPIC + "-" + topic;
    }

    public boolean getListenerStatus() {
        return true;
    }

}
package com.sinsz.im.commonly;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 通信通道管理
 * @author chenjianbo
 * @date 2017/4/15
 */
public class ChannelManager {

    private static final Logger log = LoggerFactory.getLogger(ChannelManager.class);

    private ChannelManager() {}

    private static class ChannelManagerHandler {
        private static ChannelManager channelManager = new ChannelManager();
    }

    public static ChannelManager instance() {
        return ChannelManagerHandler.channelManager;
    }

    /**
     * 所有连接成功的客户端用户及通道信息
     */
    private static volatile Map<String, LocalChannel> channels = new ConcurrentHashMap<>();

    /**
     * 是否关闭
     */
    public volatile boolean isClose = false;

    /**
     * 获取当前通道连接数
     * @return
     */
    public long size() {
        if (channels != null && !channels.isEmpty()) {
            return channels.size();
        }
        return 0;
    }

    /**
     * 获取用户通道信息
     * @param channelId
     * @return
     */
    public LocalChannel getChannel(String channelId) {
        try {
            return channels.get(channelId);
        } catch (Exception e) {
            log.error("[ChannelManager]  Get the user channel information abnormal!Channel ID：{}", channelId);
            e.printStackTrace(System.out);
        }
        return null;
    }

    /**
     * 删除用户通道信息
     * @param channelId
     * @return
     */
    public boolean removeChannel(String channelId) {
        try {
            channels.remove(channelId);
            return true;
        } catch (Exception e) {
            log.error("[ChannelManager]  Remove abnormal user channel information!Channel ID：{}", channelId);
            e.printStackTrace(System.out);
        }
        return false;
    }

    /**
     * 判断通道是否存在
     * @param channelId
     * @return
     */
    public boolean isExist(String channelId) {
        return channels.containsKey(channelId);
    }

    /**
     * 保存通道信息
     * @param channelId
     * @param channel
     * @return
     */
    public boolean addChannel(String channelId, LocalChannel channel) {
        try {
            channels.put(channelId, channel);
            return true;
        } catch (Exception e) {
            log.error("[ChannelManager]  Save the user channel information abnormal!Channel ID: {}, channel information：{}", channelId, channel);
            e.printStackTrace(System.out);
        }
        return false;
    }

    /**
     * 更新通道信息
     * @param channelId
     * @param channel
     * @return
     */
    public boolean updateChannel(String channelId, LocalChannel channel) {
        if (isExist(channelId)) {
            removeChannel(channelId);
        }
        return addChannel(channelId, channel);
    }

}

package com.sinsz.im.commonly;


import io.netty.channel.Channel;

/**
 * 每个用户的本地通道信息
 * @author chenjianbo
 * @date 2017/4/15
 */
public class LocalChannel {

    /**
     * 用户ID
     */
    private volatile String userId;

    /**
     * 用户票根
     */
    private volatile String ticket;

    /**
     * 是否通过登录验证，true表示通过，false表示未通过
     */
    private volatile boolean verify;

    /**
     * 通道信息
     */
    private volatile Channel channel;

    /**
     * 创建时间
     */
    private volatile long createTime;

    /**
     * 最后连接时间
     */
    private volatile long lastTime;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTicket() {
        return ticket;
    }

    public void setTicket(String ticket) {
        this.ticket = ticket;
    }

    public Channel getChannel() {
        return channel;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }

    public long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(long createTime) {
        this.createTime = createTime;
    }

    public long getLastTime() {
        return lastTime;
    }

    public void setLastTime(long lastTime) {
        this.lastTime = lastTime;
    }

    public boolean isVerify() {
        return verify;
    }

    public void setVerify(boolean verify) {
        this.verify = verify;
    }
}

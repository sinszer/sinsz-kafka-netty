package com.sinsz.im.configuration.im;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Im聊天线程配置
 * @author chenjianbo
 * @date 2017/9/28
 */
@Component
@ConfigurationProperties(prefix="connector")
public class ConnectorProp {

    private String host;

    private int port;

    private int imCoreThread;

    private int imWorkThread;

    private int readerIdleTimeSeconds;

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public int getImCoreThread() {
        return imCoreThread;
    }

    public void setImCoreThread(int imCoreThread) {
        this.imCoreThread = imCoreThread;
    }

    public int getImWorkThread() {
        return imWorkThread;
    }

    public void setImWorkThread(int imWorkThread) {
        this.imWorkThread = imWorkThread;
    }

    public int getReaderIdleTimeSeconds() {
        return readerIdleTimeSeconds;
    }

    public void setReaderIdleTimeSeconds(int readerIdleTimeSeconds) {
        this.readerIdleTimeSeconds = readerIdleTimeSeconds;
    }
}

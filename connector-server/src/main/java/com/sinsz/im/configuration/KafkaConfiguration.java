package com.sinsz.im.configuration;

import com.sinsz.im.commonly.Topic;
import com.sinsz.im.configuration.kafka.ReceiveProducerListener;
import com.sinsz.im.consumer.SendMessageConsumer;
import com.sinsz.im.kafka.KafkaMessage;
import com.sinsz.im.kafka.KafkaSender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.listener.KafkaMessageListenerContainer;
import org.springframework.kafka.listener.config.ContainerProperties;

import javax.annotation.PostConstruct;

/**
 * kafka配置
 * <b>
 *     用于接收用户在连接器向逻辑器发送的消息
 *          并且
 *     用于向用户发送回执消息
 * </b>
 * @author chenjianbo
 * @date 2017/9/25
 */
@Configuration
@EnableKafka
@Order(3)
public class KafkaConfiguration {

    private static final Logger logger = LoggerFactory.getLogger(KafkaConfiguration.class);

    public KafkaConfiguration() {
        logger.info(">>>>>>>>>>>>>>>>>>>> 消息队列 >>>>>>>>>>>>>>>>>>");
    }

    //初始化服务节点主题Topic
    static {
        Topic.getInstance().initTopic();
    }

    // 初始化分区数
    @Value("${spring.kafka.partition:1}")
    private Integer partition;

    // 初始化分区数
    @PostConstruct
    public void init() {
        KafkaSender.getInstance().setPartition(partition);
    }

    /** ---------------------配置生产者--------------------- */

    @Autowired(required = false)
    private ProducerFactory<String, KafkaMessage> producerFactory;

    @Autowired(required = false)
    private ReceiveProducerListener receiveProducerListener;

    /**
     * 属性定义调用对象
     * @return
     */
    @Bean("kafkaTemplate")
    public KafkaTemplate<String, KafkaMessage> kafkaTemplate() {
        KafkaTemplate<String, KafkaMessage> kafkaTemplate = new KafkaTemplate<>(producerFactory);
        kafkaTemplate.setProducerListener(receiveProducerListener);
        return kafkaTemplate;
    }

    /** ---------------------配置消费者监听--------------------- */

    @Autowired(required = false)
    private ConsumerFactory<String, KafkaMessage> consumerFactory;

    @Autowired(required = false)
    private SendMessageConsumer sendMessageConsumer;

    /**
     * 实现监听
     * @return
     */
    @Bean
    @Order(1)
    public KafkaMessageListenerContainer<String, KafkaMessage> kafkaMessageListenerContainer() {
        ContainerProperties properties = new ContainerProperties(
                Topic.getInstance().getTopic()
        );
        properties.setMessageListener(sendMessageConsumer);
        return new KafkaMessageListenerContainer<>(
                consumerFactory,
                properties
        );
    }

}
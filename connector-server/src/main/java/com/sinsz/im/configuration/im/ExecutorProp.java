package com.sinsz.im.configuration.im;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 线程池配置文件
 * @author chenjianbo
 * @date 2017/9/28
 */
@Component
@ConfigurationProperties(prefix="executor")
public class ExecutorProp {

    private int corePoolSize;

    private int maximumPoolSize;

    private int keepAliveTime;

    private int schCorePoolSize;

    public int getCorePoolSize() {
        return corePoolSize;
    }

    public void setCorePoolSize(int corePoolSize) {
        this.corePoolSize = corePoolSize;
    }

    public int getMaximumPoolSize() {
        return maximumPoolSize;
    }

    public void setMaximumPoolSize(int maximumPoolSize) {
        this.maximumPoolSize = maximumPoolSize;
    }

    public int getKeepAliveTime() {
        return keepAliveTime;
    }

    public void setKeepAliveTime(int keepAliveTime) {
        this.keepAliveTime = keepAliveTime;
    }

    public int getSchCorePoolSize() {
        return schCorePoolSize;
    }

    public void setSchCorePoolSize(int schCorePoolSize) {
        this.schCorePoolSize = schCorePoolSize;
    }
}

package com.sinsz.im.configuration;

import com.sinsz.im.commonly.Topic;
import com.sinsz.im.commonly.WorkThreadFactory;
import com.sinsz.im.configuration.im.ConnectorProp;
import com.sinsz.im.configuration.im.ExecutorProp;
import com.sinsz.im.handler.TcpHandler;
import com.sinsz.im.protocol.MessageDecode;
import com.sinsz.im.protocol.MessageEncode;
import com.sinsz.im.repository.RegConnectorRepository;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.timeout.IdleStateHandler;
import kafka.admin.AdminUtils;
import kafka.utils.ZkUtils;
import org.apache.kafka.common.security.JaasUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.core.annotation.Order;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.Map;
import java.util.concurrent.*;

/**
 * CM管理器 相关初始化操作
 * @author chenjianbo
 * @date 2017/9/28
 */
@Configuration
@Scope(value= BeanDefinition.SCOPE_SINGLETON)
@Order(4)
public class InitConnectorConfiguration {

    @Autowired(required = false)
    private ConnectorProp connectorProp;

    @Autowired(required = false)
    private ExecutorProp executorProp;

    /**
     * 开启线程组（主线程）
     * @return
     */
    @Bean
    public EventLoopGroup bossGroup() {
        return new NioEventLoopGroup(
                connectorProp.getImCoreThread(),
                new WorkThreadFactory("BOSS-")
        );
    }

    /**
     * 开启线程组（辅助线程）
     * @return
     */
    @Bean
    public EventLoopGroup workerGroup() {
        return new NioEventLoopGroup(
                Runtime.getRuntime().availableProcessors() * connectorProp.getImWorkThread(),
                new WorkThreadFactory("WORK-")
        );
    }

    /**
     * 开启线程池
     * @return
     */
    @Bean
    public ExecutorService executorService() {
        return new ThreadPoolExecutor(
                executorProp.getCorePoolSize(),
                executorProp.getMaximumPoolSize(),
                executorProp.getKeepAliveTime(),
                TimeUnit.SECONDS,
                new LinkedBlockingQueue<>(),
                new WorkThreadFactory("EXE")
        );
    }

    /**
     * 开启定时任务线程池
     * @return
     */
    @Bean
    public ScheduledExecutorService scheduledExecutorService() {
        return Executors.newScheduledThreadPool(
                executorProp.getSchCorePoolSize(),
                new WorkThreadFactory("SCH")
        );
    }

    // =========================================================================================
    // =================================== 初始化IM服务 =========================================
    // =========================================================================================

    private static final Logger logger = LoggerFactory.getLogger(InitConnectorConfiguration.class);

    @Autowired
    private EventLoopGroup bossGroup;

    @Autowired
    private EventLoopGroup workerGroup;

    @Autowired
    private ExecutorService executorService;

    @Autowired
    private ScheduledExecutorService scheduledExecutorService;

    @Autowired(required = false)
    private TcpHandler tcpHandler;

    @Autowired(required = false)
    private RegConnectorRepository regConnectorRepository;

    @Value("${spring.kafka.zookeeper-servers:127.0.0.1:2181}")
    private String zookeeperServices;

    /**
     * 绑定Tcp服务
     */
    private void binding() {
        ServerBootstrap bootstrap = new ServerBootstrap();
        //基本设置
        bootstrap.group(bossGroup, workerGroup);
        bootstrap.channel(NioServerSocketChannel.class);
        bootstrap.childOption(ChannelOption.SO_KEEPALIVE, true);
        bootstrap.childOption(ChannelOption.TCP_NODELAY, true); //设置一次大数据封包
        bootstrap.option(ChannelOption.SO_BACKLOG, 128);
        //设置编解码已经业务处理器
        bootstrap.childHandler(new ChannelInitializer<SocketChannel>() {
            @Override
            protected void initChannel(SocketChannel ch) throws Exception {
                ch.pipeline()
                        .addLast("encoder", new MessageEncode())
                        .addLast("decoder", new MessageDecode())
                        .addLast("keepAlive",
                                new IdleStateHandler(
                                        connectorProp.getReaderIdleTimeSeconds(),
                                        0,
                                        0
                                )
                        )
                        .addLast("handler", tcpHandler);
            }
        });
        //开启服务监听
        try {
            //绑定端口，同步等待成功
            ChannelFuture future = bootstrap.bind(connectorProp.getPort()).sync();
            //等待服务器监听端口关闭你
            future.channel().closeFuture().sync();
        } catch (InterruptedException e) {
            logger.error("载入bootstrap异常 " ,e.getLocalizedMessage());
            e.printStackTrace(System.out);
        } finally {
            //退出，释放线程池资源
            bossGroup.shutdownGracefully();
            workerGroup.shutdownGracefully();
        }
    }

    /**
     * 删除旧的主题
     * @param topic
     */
    private void deleteTopic(String topic) {
        ZkUtils zkUtils = null;
        try {
            // 连接zookeeper
            zkUtils = ZkUtils.apply(
                    zookeeperServices, 30000, 30000,
                    JaasUtils.isZkSecurityEnabled()
            );
            AdminUtils.deleteTopic(zkUtils, topic);
        } catch (Exception e) {
            e.printStackTrace(System.out);
            logger.error("连接与操作删除主题Topic时异常， e: {}", e);
        } finally {
            if (zkUtils != null) {
                zkUtils.close();
            }
        }
    }

    /**
     * 注册服务器连接相关
     */
    private void regConnector() {
        String ip = connectorProp.getHost();
        int port = connectorProp.getPort();
        String address = ip + ":" + String.valueOf(port);
        String topic = Topic.getInstance().getTopic();
        Map<Object, Object> map = regConnectorRepository.getAll();
        if (map == null) {
            regConnectorRepository.insert(ip, port, topic);
        } else {
           Boolean exist = Boolean.FALSE;
            for (Map.Entry<Object, Object> entry : map.entrySet()) {
                String hKey = (String) entry.getKey();
                String hValue = (String) entry.getValue();
                if (hKey.equals(address)) {
                    exist = Boolean.TRUE;
                    if (!hValue.equals(topic)) {
                        //删除hValue(删除旧主题)
                        deleteTopic(hValue);
                    }
                    regConnectorRepository.update(ip, port, topic);
                }
            }
            if (!exist) {
                regConnectorRepository.insert(ip, port, topic);
            }
        }
    }

    /**
     * 初始化Netty服务
     */
    @PostConstruct
    public void init() {
        //绑定TCP服务
        executorService.execute(() -> {
            if (logger.isDebugEnabled()) {
                logger.debug("启动TCP连接服务( {} )", DateTime.now().toString("yyyy-MM-dd HH:mm:ss.SSS"));
            }
            binding();
        });
        //注册CM信息
        scheduledExecutorService.scheduleWithFixedDelay(() -> {
            if (logger.isDebugEnabled()) {
                logger.debug("启动CM注册服务( {} )", DateTime.now().toString("yyyy-MM-dd HH:mm:ss.SSS"));
            }
            regConnector();
        },3, 70, TimeUnit.SECONDS);
    }

    /**
     * 正常停机执行
     */
    @PreDestroy
    public void stopTcp() {
        bossGroup.shutdownGracefully();
        workerGroup.shutdownGracefully();
        executorService.shutdown();
        scheduledExecutorService.shutdown();
    }

}
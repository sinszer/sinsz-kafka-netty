package com.sinsz.im.configuration.kafka;

import com.sinsz.im.common.SpringContextHolder;
import com.sinsz.im.commonly.ChannelManager;
import com.sinsz.im.commonly.Topic;
import com.sinsz.im.event.KafkaEvent;
import com.sinsz.im.event.KafkaObject;
import com.sinsz.im.kafka.KafkaMessage;
import com.sinsz.im.protocol.SMessage;
import com.sinsz.im.repository.KafkaReceiveFailRepository;
import com.sinsz.im.repository.KafkaReceiveSuccessRepository;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.support.ProducerListener;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * kafka生产者监听
 * <b>
 *     处理成功与失败的机制
 * </b>
 * @author chenjianbo
 * @date 2017/9/26
 */
@Component
public class ReceiveProducerListener implements ProducerListener<String, KafkaMessage> {

    private static final Logger logger = LoggerFactory.getLogger(ReceiveProducerListener.class);

    @Autowired
    private KafkaReceiveFailRepository kafkaReceiveFailRepository;

    @Autowired
    private KafkaReceiveSuccessRepository kafkaReceiveSuccessRepository;

    /**
     * 发送成功调用
     * @param topic
     * @param partition
     * @param key
     * @param value
     * @param recordMetadata
     */
    @Override
    public void onSuccess(String topic, Integer partition, String key,
                          KafkaMessage value,
                          RecordMetadata recordMetadata) {
        if (logger.isDebugEnabled()) {
            logger.debug("时间：{} - 通道：{} - \n成功发送消息：{}",
                    new Date(), value.getChannelId(), value.getMessageBuf());
        }
        // 清理掉失败统计
        if (kafkaReceiveFailRepository.exist(key)) {
            kafkaReceiveFailRepository.delete(key);
            logger.debug("清理成功！key:{}", key);
        }
        // 记录信息到未读成功命令集合中(暂时只作存储，存储3天，3天无命令往来则自动销毁)
        kafkaReceiveSuccessRepository.insert(
                value.getToken(),
                value.getUserId(),
                value.getMessageBuf()
        );
    }

    /**
     * 发送失败调用,首先处理重发机制3次重发，
     * 3次结束仍然失败则丢弃并回发信息
     * @param topic
     * @param partition
     * @param key
     * @param value
     * @param exception
     */
    @Override
    public void onError(String topic, Integer partition, String key,
                        KafkaMessage value, Exception exception) {
        int retryNum = kafkaReceiveFailRepository.get(key);
        // 多次后的回执与清理
        if (retryNum >= 3) {
            if (logger.isDebugEnabled()) {
                logger.debug("时间：{} - 通道：{} - \n重发3次消息失败：{}",
                        new Date(), value.getChannelId(), value.getMessageBuf());
            }
            returnReceipt(value);
            kafkaReceiveFailRepository.delete(key);
            return;
        }
        // 实现记录并重发
        if (logger.isDebugEnabled()) {
            logger.debug("时间：{} - 通道：{} - \n第{}次重新发送消息：{}",
                    new Date(), value.getChannelId(), retryNum + 1, value.getMessageBuf());
        }
        if (retryNum == 0) {
            kafkaReceiveFailRepository.insert(key);
        } else {
            kafkaReceiveFailRepository.update(key);
        }
        //启用重发监听
        KafkaObject object = new KafkaObject(topic, partition, key, value);
        SpringContextHolder.getApplicationContext().publishEvent(
                new KafkaEvent<>(object)
        );
    }

    /**
     * 需要验证成功或失败设置为true，false表示不感兴趣
     * <p>
     *     ps：一旦设置为false则会停止监听的所有逻辑处理。
     * </p>
     * @return
     */
    @Override
    public boolean isInterestedInSuccess() {
        return Topic.getInstance().getListenerStatus();
    }

    /**
     * 多次失败后的回执处理
     * @param message
     */
    private void returnReceipt(KafkaMessage message) {
        switch (message.getMessageBuf().getRequest()) {
            case LOGIN:
                response(SMessage.MessageBuf.RequestCommandEnum.RESPLOGIN,
                        "登录失败", message.getChannelId());
                //登录情况多次请求不到需要关闭连接
                ChannelManager.instance()
                        .getChannel(message.getChannelId())
                        .getChannel().close();
                break;
            case SENDINFO:
                response(SMessage.MessageBuf.RequestCommandEnum.RESPSENDINFO,
                        "消息发送失败",message.getChannelId());
                break;
            case RESPNOTICE:
                response(SMessage.MessageBuf.RequestCommandEnum.NOTICE,
                        "接收通知失败",message.getChannelId());
                break;
            case RESPRECEIVE:
                response(SMessage.MessageBuf.RequestCommandEnum.RECEIVE,
                        "信息响应失败",message.getChannelId());
                break;
            default:
        }
    }

    private void response(SMessage.MessageBuf.RequestCommandEnum rce, String msg, String channelId) {
        SMessage.MessageBuf result = SMessage.MessageBuf.newBuilder()
                .setRequest(rce)
                .setStatus(
                        SMessage.MessageBuf.Status.newBuilder()
                                .setState(false)
                                .setMessage(msg + "，服务端队列异常")
                                .setServerTime(System.currentTimeMillis())
                                .build()
                ).build();
        ChannelManager.instance().getChannel(channelId)
                .getChannel().writeAndFlush(result);
    }

}
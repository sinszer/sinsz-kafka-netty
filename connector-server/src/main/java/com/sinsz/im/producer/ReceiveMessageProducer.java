package com.sinsz.im.producer;

import com.sinsz.im.commonly.ChannelManager;
import com.sinsz.im.commonly.Topic;
import com.sinsz.im.kafka.KafkaCommand;
import com.sinsz.im.kafka.KafkaMessage;
import com.sinsz.im.kafka.KafkaSender;
import com.sinsz.im.protocol.SMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

/**
 * netty对用户发送上来消息的生产
 * @author chenjianbo
 * @date 2017/9/27
 */
@Component
public class ReceiveMessageProducer {

    @Autowired
    private KafkaTemplate<String, KafkaMessage> kafkaTemplate;

    /**
     * 监听中的重发->向队列中重新发送
     * @param topic
     * @param partition
     * @param key
     * @param value
     */
    public void listenerRetry(
            String topic,
            Integer partition,
            String key,
            KafkaMessage value){
        KafkaSender.getInstance().sender(
                kafkaTemplate,
                topic,
                key,
                partition,
                value
        );
    }

    /**
     * 登录信息组装
     * 生产登录验证信息到队列
     * @param channelId
     * @param buf
     */
    public void login(
            String channelId,
            SMessage.MessageBuf buf) {
        commonly(channelId, buf, null);
    }

    /**
     * 公共的生成消息方法
     * @param channelId
     * @param buf
     */
    public void commonly(String channelId,
                         SMessage.MessageBuf buf, String body) {
        KafkaMessage msg = new KafkaMessage(
                Topic.getInstance().getTopic(),
                KafkaCommand.MESSAGE,
                body,
                System.currentTimeMillis(),
                ChannelManager.instance().getChannel(channelId).getUserId(),
                channelId,
                buf,
                buf.getToken()
        );
        KafkaSender.getInstance().sendToReceiveTopic(
                kafkaTemplate,
                msg
        );
    }

}
package com.sinsz.im.handler;

import com.sinsz.im.adapter.*;
import com.sinsz.im.common.SpringContextHolder;
import com.sinsz.im.protocol.SMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * 消息工厂
 */
@Component
public class MessageBufFactory {

    private static final Logger logger = LoggerFactory.getLogger(MessageBufFactory.class);

    public IAdapter getAdapter(SMessage.MessageBuf messageBuf) {
        switch (messageBuf.getRequest()) {
            /**
             * 登录 / 用户发起
             */
            case LOGIN: return SpringContextHolder.getBean(LoginAdapterImpl.class);
            case RESPLOGIN: break;
            /**
             * 心跳 / 用户发起
             */
            case KEEPHEART: return SpringContextHolder.getBean(KeepHeartAdapterImpl.class);
            case RESPKEEPHEART: break;
            /**
             * 登出 / 用户发起
             */
            case LOGOUT: return SpringContextHolder.getBean(LogoutAdapterImpl.class);
            case RESPLOGOUT: break;
            /**
             * 接收用户发送 / 用户发起
             */
            case SENDINFO: return SpringContextHolder.getBean(SendInfoAdapterImpl.class);
            case RESPSENDINFO: break;
            /**
             * 通知用户结果 / 服务器发起
             */
            case NOTICE: break;
            case RESPNOTICE: return SpringContextHolder.getBean(RespNoticeAdapterImpl.class);
            /**
             * 发送用户接收 / 服务器发起
             */
            case RECEIVE: break;
            case RESPRECEIVE: return SpringContextHolder.getBean(RespReceiveAdapterImpl.class);
        }
        logger.error("该命令尚未接入。");
        return null;
    }

}
package com.sinsz.im.handler;


import com.sinsz.im.adapter.IAdapter;
import com.sinsz.im.commonly.ChannelManager;
import com.sinsz.im.commonly.LocalChannel;
import com.sinsz.im.commonly.Topic;
import com.sinsz.im.exception.SZError;
import com.sinsz.im.exception.SZException;
import com.sinsz.im.protocol.SMessage;
import com.sinsz.im.repository.MultiTokenRepository;
import com.sinsz.im.repository.TokenRepository;
import com.sinsz.im.repository.support.Token;
import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


/**
 * 消息处理器
 * @author chenjianbo
 */
@Component
@Sharable
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class TcpHandler extends ChannelInboundHandlerAdapter {

    private static final Logger log = LoggerFactory.getLogger(TcpHandler.class);

    @Autowired
    private MessageBufFactory messageBufFactory;

    @Autowired
    private MultiTokenRepository multiTokenRepository;

    @Autowired
    private TokenRepository tokenRepository;

    /**
     * 接收到消息的处理
     * @param ctx
     * @param msg
     * @throws Exception
     */
    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        String channelId = ctx.channel().id().toString();
        if (msg == null || !(msg instanceof SMessage.MessageBuf)) {
            log.error("消息内容不能为空，通道: {}", channelId);
            return ;
        }
        if (ChannelManager.instance().isClose) {
            log.error("聊天服务正在维护，时间: {}", new Date());
            return ;
        }
        //消息对象转换为协议对象
        SMessage.MessageBuf msgBuf = (SMessage.MessageBuf) msg;
        /**
         * 客户端之间的通信处理
         * 获取消息处理器
         */
        IAdapter adapter = messageBufFactory.getAdapter(msgBuf);
        if (adapter != null) {
            //权限验证
            if (adapter.verify()) {
                //验证通道是否合法
                LocalChannel localChannel = ChannelManager.instance().getChannel(channelId);
                if (localChannel == null || !localChannel.isVerify()) {
                    log.error("用户验证失败，通道: {}, 本地缓存: {}", channelId, localChannel);
                    throw new SZException(SZError.USER_NOT_LOGIN);
                }
                //验证是否具有票据
                if (StringUtils.isEmpty(msgBuf.getTicket())
                        || !localChannel.getTicket().equals(msgBuf.getTicket())) {
                    log.error("用户票据不合法，通道: {}, 用户ID: {}", channelId, localChannel.getUserId());
                    throw new SZException(SZError.USER_TICKET_ERROR);
                }
            }
            //业务执行
            adapter.invoker(ctx, msgBuf);
            return ;
        }
        log.error("没有找到消息处理类型... {}", msgBuf.getRequest());
    }

    /**
     * 清理关闭时执行
     * @param ctx
     * @throws Exception
     */
    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        String channelId = ctx.channel().id().toString();
        //清理用户客户端缓存
        LocalChannel channel = ChannelManager.instance().getChannel(channelId);
        if (channel != null) {
            if (log.isDebugEnabled()) {
                log.info("通道缓存清理 ! 通道: {}",channelId);
            }
            String userId = channel.getUserId();
            ChannelManager.instance().removeChannel(channelId);
            resetTopicInfo(userId);
        }
        super.channelInactive(ctx);
    }

    /**
     * 异常处理
     * @param ctx
     * @param cause
     * @throws Exception
     */
    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        String channelId = ctx.channel().id().toString();
        LocalChannel localChannel = ChannelManager.instance().getChannel(channelId);
        SMessage.MessageBuf buf = SMessage.MessageBuf.newBuilder()
                .setRequest(SMessage.MessageBuf.RequestCommandEnum.RESPLOGOUT)
                .setStatus(
                        SMessage.MessageBuf.Status.newBuilder()
                        .setMessage(cause.getMessage())
                        .setState(true)
                        .setServerTime(System.currentTimeMillis())
                ).build();
        ctx.channel().writeAndFlush(buf);
        if (localChannel != null) {
            resetTopicInfo(localChannel.getUserId());
        }
        log.error("异常信息! details: {}", cause.getLocalizedMessage());
        if (log.isDebugEnabled()) {
            cause.printStackTrace(System.out);
        }
        ctx.close();
    }

    /**
     * 当清理掉TCP通道时，要更新掉当前这个通道在登录信息中的队列信息
     * @param userId
     */
    private void resetTopicInfo(String userId) {
        try {
            if (!StringUtils.isEmpty(userId)) {
                List<String> list = multiTokenRepository.getAll(userId)
                        .stream().map(o -> (String)o)
                        .collect(Collectors.toList());
                if (list != null) {
                    list.forEach(token -> {
                        Token tk = tokenRepository.get(token);
                        if (tk != null && Topic.getInstance().getTopic()
                                .equals(tk.getTopic())) {
                            tk.setChannelId("");
                            tk.setTopic("");
                            tokenRepository.insert(token, tk);
                        }
                    });
                }
                log.info("登录队列已重置...");
            }
        } catch (Exception e) {
            log.error("由服务器主动发起的关闭导致的异常...... , 后果忽略");
        }
    }
}
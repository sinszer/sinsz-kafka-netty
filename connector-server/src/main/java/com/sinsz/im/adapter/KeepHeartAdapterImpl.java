package com.sinsz.im.adapter;

import com.sinsz.im.commonly.ChannelManager;
import com.sinsz.im.commonly.LocalChannel;
import com.sinsz.im.protocol.SMessage;
import io.netty.channel.ChannelHandlerContext;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * 心跳监听
 * <p>
 *     前端超过2秒没有收到回执应该关闭掉连接
 * </p>
 * @author chenjianbo
 * @date 2017/9/29
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class KeepHeartAdapterImpl implements IAdapter {
    @Override
    public void invoker(ChannelHandlerContext ctx, SMessage.MessageBuf message) {
        if (ChannelManager.instance().getChannel(
                ctx.channel().id().toString()) != null) {
            SMessage.MessageBuf buf = SMessage.MessageBuf
                    .newBuilder()
                    .setRequest(SMessage.MessageBuf.RequestCommandEnum.RESPKEEPHEART)
                    .setStatus(
                            SMessage.MessageBuf.Status
                            .newBuilder()
                            .setState(true)
                            .setMessage("PONG")
                            .setServerTime(System.currentTimeMillis())
                    ).build();
            LocalChannel channel = ChannelManager.instance()
                    .getChannel(ctx.channel().id().toString());
            channel.setLastTime(System.currentTimeMillis());
            ChannelManager.instance().updateChannel(
                    ctx.channel().id().toString(),
                    channel
            );
            channel.getChannel().writeAndFlush(buf);
        } else {
            ctx.channel().close();
        }
    }

    @Override
    public boolean verify() {
        return true;
    }
}

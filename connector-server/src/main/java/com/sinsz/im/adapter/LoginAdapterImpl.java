package com.sinsz.im.adapter;

import com.alibaba.fastjson.JSON;
import com.sinsz.im.commonly.ChannelManager;
import com.sinsz.im.commonly.LocalChannel;
import com.sinsz.im.producer.ReceiveMessageProducer;
import com.sinsz.im.protocol.SMessage;
import io.netty.channel.ChannelHandlerContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

/**
 * 登录监听
 * <p>
 *     业务系统登录也需要处理踢端逻辑
 * </p>
 * @author chenjianbo
 * @date 2017/9/29
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class LoginAdapterImpl implements IAdapter {

    @Autowired
    private ReceiveMessageProducer receiveMessageProducer;

    private static final Logger logger = LoggerFactory.getLogger(LoginAdapterImpl.class);

    @Override
    public void invoker(ChannelHandlerContext ctx, SMessage.MessageBuf message) {
        if (ObjectUtils.isEmpty(message.getToken())) {
            SMessage.MessageBuf result = SMessage.MessageBuf
                    .newBuilder()
                    .setRequest(SMessage.MessageBuf.RequestCommandEnum.RESPLOGIN)
                    .setStatus(
                            SMessage.MessageBuf.Status.newBuilder()
                            .setState(false)
                            .setServerTime(System.currentTimeMillis())
                            .setMessage("用户尚未登录系统").build()
                    ).build();
            ctx.channel().writeAndFlush(result);
            ctx.channel().close();
            return;
        }
        String channelId = ctx.channel().id().toString();
        //初始化存储
        LocalChannel channel = new LocalChannel();
        channel.setVerify(false);
        channel.setUserId("");
        channel.setTicket("");
        channel.setCreateTime(System.currentTimeMillis());
        channel.setLastTime(System.currentTimeMillis());
        channel.setChannel(ctx.channel());
        ChannelManager.instance().addChannel(channelId, channel);
        if (logger.isDebugEnabled()) {
            logger.debug("缓存信息 -> LocalChannel：{}", JSON.toJSON(channel));
        }
        //发送队列消息
        receiveMessageProducer.login(channelId, message);
    }

    @Override
    public boolean verify() {
        return false;
    }
}

package com.sinsz.im.adapter;

import com.sinsz.im.commonly.ChannelManager;
import com.sinsz.im.producer.ReceiveMessageProducer;
import com.sinsz.im.protocol.SMessage;
import io.netty.channel.ChannelHandlerContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * 用户接收到消息后的回执
 * @author chenjianbo
 * @date 2017/9/29
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class RespReceiveAdapterImpl implements IAdapter {

    @Autowired
    private ReceiveMessageProducer receiveMessageProducer;

    @Override
    public void invoker(ChannelHandlerContext ctx, SMessage.MessageBuf message) {
        if (message.getMessageId() <= 0) {
            SMessage.MessageBuf result = SMessage.MessageBuf.newBuilder()
                    .setRequest(SMessage.MessageBuf.RequestCommandEnum.RECEIVE)
                    .setStatus(
                            SMessage.MessageBuf.Status.newBuilder()
                                    .setState(false)
                                    .setMessage("消息ID不合法")
                                    .setServerTime(System.currentTimeMillis())
                                    .build()
                    ).build();
            ctx.channel().writeAndFlush(result);
            return;
        }
        message = message.toBuilder()
                .setStatus(
                        message.getStatus().toBuilder()
                                .setServerTime(System.currentTimeMillis()).build()
                ).build();
        String channelId = ctx.channel().id().toString();
        receiveMessageProducer.commonly(channelId, message, "responseReceive To Topic");
    }

    @Override
    public boolean verify() {
        return true;
    }
}

package com.sinsz.im.adapter;

import com.sinsz.im.commonly.ChannelManager;
import com.sinsz.im.commonly.LocalChannel;
import com.sinsz.im.protocol.SMessage;
import io.netty.channel.ChannelHandlerContext;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * 登出监听
 * @author chenjianbo
 * @date 2017/9/29
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class LogoutAdapterImpl implements IAdapter {
    @Override
    public void invoker(ChannelHandlerContext ctx, SMessage.MessageBuf message) {
        LocalChannel channel = ChannelManager.instance().getChannel(
                ctx.channel().id().toString()
        );
        if (channel != null) {
            SMessage.MessageBuf buf = SMessage.MessageBuf.newBuilder()
                    .setRequest(SMessage.MessageBuf.RequestCommandEnum.RESPLOGOUT)
                    .setStatus(
                            SMessage.MessageBuf.Status.newBuilder()
                            .setState(true)
                            .setMessage("退出登录")
                            .setServerTime(System.currentTimeMillis())
                    ).build();
            ctx.channel().writeAndFlush(buf);
        }
        ctx.channel().close();
    }

    @Override
    public boolean verify() {
        return false;
    }
}

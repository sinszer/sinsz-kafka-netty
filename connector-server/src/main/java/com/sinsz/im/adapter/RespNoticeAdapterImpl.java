package com.sinsz.im.adapter;

import com.sinsz.im.producer.ReceiveMessageProducer;
import com.sinsz.im.protocol.SMessage;
import io.netty.channel.ChannelHandlerContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * 用户接收到通知拉取的回执
 * @author chenjianbo
 * @date 2017/9/29
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class RespNoticeAdapterImpl implements IAdapter {

    @Autowired
    private ReceiveMessageProducer receiveMessageProducer;

    @Override
    public void invoker(ChannelHandlerContext ctx, SMessage.MessageBuf message) {
        message = message.toBuilder()
                .setStatus(
                        message.getStatus().toBuilder()
                        .setServerTime(System.currentTimeMillis()).build()
                ).build();
        String channelId = ctx.channel().id().toString();
        receiveMessageProducer.commonly(channelId, message, "responseNotice To Topic");
    }

    @Override
    public boolean verify() {
        return true;
    }
}

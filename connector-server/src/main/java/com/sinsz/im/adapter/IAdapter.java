package com.sinsz.im.adapter;

import com.sinsz.im.protocol.SMessage;
import io.netty.channel.ChannelHandlerContext;

public interface IAdapter {

    void invoker(ChannelHandlerContext ctx, SMessage.MessageBuf message);

    boolean verify();

}
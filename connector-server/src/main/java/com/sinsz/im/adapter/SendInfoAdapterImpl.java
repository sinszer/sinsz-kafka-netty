package com.sinsz.im.adapter;

import com.sinsz.im.commonly.ChannelManager;
import com.sinsz.im.commonly.LocalChannel;
import com.sinsz.im.producer.ReceiveMessageProducer;
import com.sinsz.im.protocol.SMessage;
import io.netty.channel.ChannelHandlerContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

/**
 * 用户发送的消息
 * @author chenjianbo
 * @date 2017/9/29
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class SendInfoAdapterImpl implements IAdapter {

    @Autowired
    private ReceiveMessageProducer receiveMessageProducer;

    private static final Logger logger = LoggerFactory.getLogger(SendInfoAdapterImpl.class);

    @Override
    public void invoker(ChannelHandlerContext ctx, SMessage.MessageBuf message) {
        String channelId = ctx.channel().id().toString();
        LocalChannel channel = ChannelManager.instance().getChannel(channelId);
        //验证参数合法性
        long sequence = message.getSequence();
        if (sequence <= 0) {
            sendInfoFail(channel, "用户消息序列编号不合法，当前为【" + sequence + "】");
            return;
        }
        SMessage.MessageBuf.Message msg = message.getMessage();
        if (msg == null) {
            sendInfoFail(channel, "消息内容不能为空");
            return;
        }
        SMessage.MessageBuf.Message.MessageEnum messageEnum = msg.getMessageEnum();
        if (messageEnum == null
                || !messageEnum.equals(SMessage.MessageBuf.Message.MessageEnum.MESSAGE)) {
            sendInfoFail(channel, "该消息类型不存在，或不是MESSAGE消息类型");
            return;
        }
        SMessage.MessageBuf.Message.ChildMessageEnum childMessageEnum = msg.getChildMessageEnum();
        SMessage.MessageBuf.Message.ChatEnum chatEnum = msg.getChatEnum();
        if (childMessageEnum == null || chatEnum == null) {
            sendInfoFail(channel, "子消息类型或聊天类型不能为空");
            return;
        }
        String fromUserId = msg.getFromUserId();
        if (StringUtils.isEmpty(fromUserId)
                || !fromUserId.equals(channel.getUserId())) {
            sendInfoFail(channel, "发送者ID为空或ID不是合法的登录用户");
            return;
        }
        String fromUserName = msg.getFromUserName();
        if (StringUtils.isEmpty(fromUserName)) {
            sendInfoFail(channel, "发送者名称不能为空");
            return;
        }
        String toUserId = msg.getToUserId();
        if (StringUtils.isEmpty(toUserId)
                && !childMessageEnum.equals(SMessage.MessageBuf.Message.ChildMessageEnum.GROUP)) {
            sendInfoFail(channel, "接收用户ID不能为空");
            return;
        }
        if (toUserId.equals(fromUserId)) {
            sendInfoFail(channel, "不能向自己发送消息");
            return;
        }
        String toGroupId = msg.getToGroupId();
        if (StringUtils.isEmpty(toGroupId)
                && childMessageEnum.equals(SMessage.MessageBuf.Message.ChildMessageEnum.GROUP)) {
            sendInfoFail(channel, "群聊ID不能为空");
            return;
        }
        //更新内容
        SMessage.MessageBuf sendToTopicMessage = message.toBuilder()
                .setMessage(
                        message.getMessage().toBuilder()
                        .setServerTime(System.currentTimeMillis()).build()
                ).build();
        //发送到队列
        receiveMessageProducer.commonly(channelId, sendToTopicMessage, "SendInfo To Topic!");
        if (logger.isDebugEnabled()) {
            logger.debug("SendInfoAdapterImpl 接收消息成功，{}", sendToTopicMessage);
        }
    }

    @Override
    public boolean verify() {
        return true;
    }

    /**
     * 直接回执
     * @param channel
     * @param errMessage
     */
    private void sendInfoFail(LocalChannel channel, String errMessage) {
        SMessage.MessageBuf result = SMessage.MessageBuf.newBuilder()
                .setRequest(SMessage.MessageBuf.RequestCommandEnum.RESPSENDINFO)
                .setStatus(
                        SMessage.MessageBuf.Status.newBuilder()
                        .setState(false)
                        .setServerTime(System.currentTimeMillis())
                        .setMessage(errMessage).build()
                ).build();
        channel.getChannel().writeAndFlush(result);
    }

}
package com.sinsz.im.consumer;

import com.sinsz.im.commonly.ChannelManager;
import com.sinsz.im.commonly.LocalChannel;
import com.sinsz.im.kafka.KafkaMessage;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.listener.MessageListener;
import org.springframework.stereotype.Component;

/**
 * 向用户发送回执消息
 * @author chenjianbo
 * @date 2017/9/27
 */
@Component
public class SendMessageConsumer implements MessageListener<String, KafkaMessage> {

    private static final Logger logger = LoggerFactory.getLogger(SendMessageConsumer.class);

    /**
     * 监听用户发送上来的消息内容
     * 并作相应的逻辑处理
     * @param data
     */
    @Override
    public void onMessage(ConsumerRecord<String, KafkaMessage> data) {
        KafkaMessage message = data.value();
        switch (message.getCommand()) {
            case MESSAGE:
                messageFactory(message);
                break;






            default:
        }
    }

    /**
     * 处理消息类型
     * @param message
     */
    private void messageFactory(KafkaMessage message) {
        String channelId = message.getChannelId();
        LocalChannel channel = ChannelManager.instance().getChannel(channelId);
        //处理各个指令
        switch (message.getMessageBuf().getRequest()) {
            case RESPLOGIN:
                if (channel != null && channel.getChannel() != null) {
                    channel.getChannel().writeAndFlush(message.getMessageBuf());
                    //失败状态要关闭TCP通道
                    if (!message.getMessageBuf().getStatus().getState()) {
                        channel.getChannel().close();
                    } else {
                        // 登录成功补全更新本地登录用户信息
                        channel.setUserId(message.getUserId());
                        channel.setTicket(message.getMessageBuf().getStatus().getTicket());
                        channel.setVerify(true);
                        channel.setLastTime(System.currentTimeMillis());
                        ChannelManager.instance().updateChannel(channelId, channel);
                    }
                } else {
                    if (logger.isDebugEnabled()) {
                        logger.debug("未能找到数据，或已被清除");
                    }
                }
                break;
            case RESPSENDINFO:
            case NOTICE:
            case RECEIVE:
                if (channel != null && channel.getChannel() != null){
                    channel.getChannel().writeAndFlush(message.getMessageBuf());
                    channel.setLastTime(System.currentTimeMillis());
                    ChannelManager.instance().updateChannel(channelId, channel);
                }
                break;
            default:
        }
    }

}
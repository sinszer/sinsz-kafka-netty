package com.sinsz.im;

import com.sinsz.im.protocol.SMessage;
import com.sinsz.im.repository.KafkaReceiveSuccessRepository;
import com.sinsz.im.repository.SessionChatRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Set;
import java.util.UUID;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ConnectorServerApplicationTests {

	@Autowired
	private KafkaReceiveSuccessRepository kafkaReceiveSuccessRepository;

	@Autowired
	private SessionChatRepository sessionChatRepository;

	@Test
	public void contextLoads() {
	}

	@Test
	public void testSession() {
	    String from = "333";
	    String to = "222";
	    int exist = sessionChatRepository.exist(from, to);
        sessionChatRepository.insert(from, to, exist, from);
	    sessionChatRepository.insert(from, to, exist, to);

        int exist2 = sessionChatRepository.exist(to, from);
        List<String> list = sessionChatRepository.getAll(to, from, exist2);
        list.forEach(str -> {
            System.out.println(str);
            if (str.equals(from)) {
                sessionChatRepository.delete(to, from, exist2, str);
            }
        });
        list = sessionChatRepository.getAll(to, from, exist2);
        list.forEach(System.out::println);

        sessionChatRepository.delete(to, from, exist2, "555");
    }

	@Test
	public void testReceiveSuccess(){
        kafkaReceiveSuccessRepository.insert(
                "123",
                "",
                SMessage.MessageBuf.newBuilder()
                        .setToken(UUID.randomUUID().toString()).build()
        );
        kafkaReceiveSuccessRepository.insert(
                "123",
                "",
                SMessage.MessageBuf.newBuilder()
                        .setToken(UUID.randomUUID().toString()).build()
        );
        kafkaReceiveSuccessRepository.insert(
                "123",
                "",
                SMessage.MessageBuf.newBuilder()
                        .setToken(UUID.randomUUID().toString()).build()
        );

        Set<Object> set = kafkaReceiveSuccessRepository.getAll(
                "123",
                ""
        );
        if (set != null) {
            set.iterator()
                    .forEachRemaining(action -> {
                        SMessage.MessageBuf buf = (SMessage.MessageBuf)action;
                        System.out.println(buf);
                        /*kafkaReceiveSuccessRepository.delete(
                                "123",
                                "",
                                buf
                        );*/
                    });
        }
        set = kafkaReceiveSuccessRepository.getAll(
                "123",
                ""
        );
        System.out.println(set == null ? 0: set.size());
    }

}

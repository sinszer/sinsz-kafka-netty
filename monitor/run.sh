#!/usr/bin/env bash
java -cp KafkaOffsetMonitor-assembly-0.2.0.jar com.quantifind.kafka.offsetapp.OffsetGetterWeb --zk 127.0.0.1:2181 --port 8999 --refresh 10.seconds --retain 2.days &
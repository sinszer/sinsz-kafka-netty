package com.sinsz.im.dialect;

import org.hibernate.dialect.MySQLDialect;
import org.hibernate.dialect.function.SQLFunctionTemplate;
import org.hibernate.type.StringType;

/**
 * 定义Mysql方言
 */
public class MySQLExtendDialect extends MySQLDialect {

    public MySQLExtendDialect(){
        super();
        registerFunction(
                "convert_gbk",
                new SQLFunctionTemplate(StringType.INSTANCE,
                        "convert(?1 using gbk)")
        );
    }

}
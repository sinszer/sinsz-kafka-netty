package com.sinsz.im.kafka;

import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import java.util.Random;
import java.util.UUID;

import static com.sinsz.im.kafka.KafkaTopic.RECEIVE_TOPIC;

/**
 * Kafka消息发送工具
 * @author chenjianbo
 * @date 2017/9/26
 */
public class KafkaSender {

    private KafkaSender() {
    }

    private static class Sender{
        private static final KafkaSender kafkaSender = new KafkaSender();
    }

    public static KafkaSender getInstance(){
        return Sender.kafkaSender;
    }

    private static int partition;

    /**
     * 设置分区数值
     * @param partition
     */
    public void setPartition(int partition) {
        KafkaSender.partition = partition;
    }

    /**
     * 获取分区索引
     * 随机与取余分配分区方案
     * @param key
     * @return
     */
    private int getPartitionIndex(String key){
        if (StringUtils.isEmpty(key)) {
            Random random = new Random();
            return random.nextInt(partition);
        } else {
            return Math.abs(key.hashCode()) % partition;
        }
    }

    /**
     * 根据键值Hash算法随机策略分配到分区中
     * @param kafkaTemplate 发送定义
     * @param topic         队列主题
     * @param key           消息键
     * @param message       消息内容
     */
    public void defaultSend(
            KafkaTemplate<String,KafkaMessage> kafkaTemplate,
            String topic,
            String key,
            KafkaMessage message) {
        kafkaTemplate.send(
                topic,
                getPartitionIndex(key),
                key,
                message
        );
    }

    /**
     * 指定发送到分区中
     * 输入分区大于分区数时按取余处理
     * @param kafkaTemplate 发送定义
     * @param topic         队列主题
     * @param key           消息键
     * @param partitionNum  指定分区
     * @param message       消息内容
     */
    public void sender(
            KafkaTemplate<String,KafkaMessage> kafkaTemplate,
            String topic,
            String key,
            Integer partitionNum,
            KafkaMessage message) {
        partitionNum = ObjectUtils.isEmpty(partitionNum)?
                0: Math.abs(partitionNum) < partition?
                Math.abs(partitionNum): Math.abs(partitionNum) % partition;
        kafkaTemplate.send(
                topic,
                partitionNum,
                key,
                message
        );
    }

    /**
     * 发送到接收队列中
     * @param kafkaTemplate
     * @param message
     */
    public void sendToReceiveTopic(
            KafkaTemplate<String,KafkaMessage> kafkaTemplate,
            KafkaMessage message) {
        String key = UUID.randomUUID().toString().replaceAll("-", "").toLowerCase();
        defaultSend(
                kafkaTemplate,
                RECEIVE_TOPIC,
                key,
                message
        );
    }

}
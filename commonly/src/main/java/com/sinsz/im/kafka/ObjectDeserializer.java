package com.sinsz.im.kafka;

import org.apache.kafka.common.serialization.Deserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.ObjectInputStream;
import java.util.Map;

/**
 * Kafka消息反序列化
 * @author chenjianbo
 * @date 2017/9/25
 */
public class ObjectDeserializer implements Deserializer<KafkaMessage> {

    private static final Logger logger = LoggerFactory.getLogger(ObjectDeserializer.class);

    @Override
    public void configure(Map<String, ?> configs, boolean isKey) {
        //nothing to do
    }

    @Override
    public KafkaMessage deserialize(String topic, byte[] data) {
        //具有自动释放资源,前提是必须实现AutoCloseable接口
        KafkaMessage message = null;
        try (
                ByteArrayInputStream in = new ByteArrayInputStream(data);
                ObjectInputStream inputStream = new ObjectInputStream(in)
        ){
            message = (KafkaMessage)inputStream.readObject();
        } catch (Exception e) {
            e.printStackTrace(System.out);
            logger.error("kafka消息反序列化异常，e: {}", e);
        }
        return message;
    }

    @Override
    public void close() {
        //nothing to do
    }
}

package com.sinsz.im.kafka;

import org.apache.kafka.common.serialization.Serializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.Map;

/**
 * Kafka消息序列化
 * @author chenjianbo
 * @date 2017/9/25
 */
public class ObjectSerializer implements Serializer<KafkaMessage> {

    private static final Logger logger = LoggerFactory.getLogger(ObjectSerializer.class);

    @Override
    public void configure(Map<String, ?> configs, boolean isKey) {
        //nothing to do
    }

    @Override
    public byte[] serialize(String topic, KafkaMessage data) {
        //具有自动释放资源,前提是必须实现AutoCloseable接口
        try (
                ByteArrayOutputStream byteArray = new ByteArrayOutputStream();
                ObjectOutputStream outputStream = new ObjectOutputStream(byteArray)
        ) {
            outputStream.writeObject(data);
            outputStream.flush();
            return byteArray.toByteArray();
        } catch (IOException e) {
            e.printStackTrace(System.out);
            logger.error("kafka消息序列化异常，e: {}", e);
        }
        return new byte[0];
    }

    @Override
    public void close() {
        //nothing to do
    }
}

package com.sinsz.im.kafka;

/**
 * 消息队列常用命令
 * @author chenjianbo
 * @date 2017/9/29
 */
public enum KafkaCommand {
    /**
     * 普通消息
     */
    MESSAGE,
    /**
     * 统计CM连接用户数
     */
    COUNT,
    /**
     * 平台发送推送
     */
    PUSH,
    /**
     * 平台发送事件
     */
    EVENT,
    /**
     * 关闭聊天服务器
     * ChannelManager.instance().isClose = true
     */
    CLOSE_CM,
    /**
     * 启动聊天服务器
     * ChannelManager.instance().isClose = false
     */
    ON_CM
}

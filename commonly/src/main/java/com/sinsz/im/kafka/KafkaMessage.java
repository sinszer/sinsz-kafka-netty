package com.sinsz.im.kafka;

import com.sinsz.im.protocol.SMessage;
import org.springframework.util.StringUtils;

import java.io.Serializable;


/**
 * Kafka消息传输协议
 * @author chenjianbo
 * @date 2017/9/25
 */
public class KafkaMessage implements Serializable {

    private static final long serialVersionUID = 5716099108931766223L;

    /**
     * 用户所在节点主题
     */
    private String topic;

    /**
     * 命令
     */
    private KafkaCommand command;

    /**
     * 内容
     */
    private String body;

    /**
     * 发送消息上来的时间
     */
    private long messageTime;

    /**
     * 用户ID
     */
    private String userId;

    /**
     * Tcp通道ID
     */
    private String channelId;

    /**
     * 消息内容
     */
    private SMessage.MessageBuf messageBuf;

    private String token;

    public KafkaMessage() {
    }

    /**
     * 全参数构造方法
     * @param topic
     * @param command
     * @param body
     * @param messageTime
     * @param userId
     * @param channelId
     * @param messageBuf
     * @param token
     */
    public KafkaMessage(String topic,
                        KafkaCommand command,
                        String body,
                        long messageTime,
                        String userId,
                        String channelId,
                        SMessage.MessageBuf messageBuf,
                        String token) {
        this.topic = StringUtils.isEmpty(topic)?"":topic;
        this.command = command == null?KafkaCommand.MESSAGE:command;
        this.body = StringUtils.isEmpty(body)?"":body;
        this.messageTime = messageTime <= 0?System.currentTimeMillis():messageTime;
        this.userId = StringUtils.isEmpty(userId)?"":userId;
        this.channelId = StringUtils.isEmpty(channelId)?"":channelId;
        this.messageBuf = messageBuf;
        this.token = StringUtils.isEmpty(token)?"":token;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public SMessage.MessageBuf getMessageBuf() {
        return messageBuf;
    }

    public void setMessageBuf(SMessage.MessageBuf messageBuf) {
        this.messageBuf = messageBuf;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public long getMessageTime() {
        return messageTime;
    }

    public void setMessageTime(long messageTime) {
        this.messageTime = messageTime;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public KafkaCommand getCommand() {
        return command;
    }

    public void setCommand(KafkaCommand command) {
        this.command = command;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}

package com.sinsz.im.kafka;

/**
 * 用与消息队列Topic的定义
 * @author chenjianbo
 * @date 2017/9/26
 */
public interface KafkaTopic {

    //接收消息
    String RECEIVE_TOPIC = "receive-topic";

    //发送消息(前缀)
    String SEND_TOPIC = "send-topic";

}
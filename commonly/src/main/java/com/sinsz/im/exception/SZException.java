package com.sinsz.im.exception;

public class SZException  extends RuntimeException {

    private IError error;

    public SZException(IError error) {
        super(error.getCode() + ":" + error.getMessage());
        this.error = error;
    }

    public IError getError() {
        return error;
    }

    public void setError(IError error) {
        this.error = error;
    }

    @Override
    public String toString() {
        return error.getCode() + ":" + error.getMessage();
    }

}
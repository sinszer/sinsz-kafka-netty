package com.sinsz.im.exception;

public interface IError {

    String getCode();

    String getMessage();
}
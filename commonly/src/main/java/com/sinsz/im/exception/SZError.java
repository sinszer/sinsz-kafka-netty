package com.sinsz.im.exception;

public enum SZError implements IError {

    SYSTEM_ERR("system-err", "系统异常"),
    SYSTEM_SQL_ERR("system-sql-err", "数据执行异常"),


    USER_NOT_LOGIN("im-user-001","用户尚未登录"),
    USER_TICKET_ERROR("im-user-002","用户票据不合法"),

    ;

    private String code;

    private String message;

    SZError(String code, String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public String getCode() {
        return this.code;
    }

    @Override
    public String getMessage() {
        return this.message;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}

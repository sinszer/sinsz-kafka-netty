package com.sinsz.im.event;

import com.sinsz.im.kafka.KafkaMessage;

import java.io.Serializable;

/**
 * @author chenjianbo
 */
public class KafkaObject implements Serializable {

    private static final long serialVersionUID = 7768633968906658826L;
    private String topic;
    private Integer partition;
    private String key;
    private KafkaMessage value;

    public KafkaObject() {
    }

    public KafkaObject(String topic, Integer partition, String key, KafkaMessage value) {
        this.topic = topic;
        this.partition = partition;
        this.key = key;
        this.value = value;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public Integer getPartition() {
        return partition;
    }

    public void setPartition(Integer partition) {
        this.partition = partition;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public KafkaMessage getValue() {
        return value;
    }

    public void setValue(KafkaMessage value) {
        this.value = value;
    }
}

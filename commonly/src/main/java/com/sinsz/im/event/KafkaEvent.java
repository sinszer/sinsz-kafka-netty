package com.sinsz.im.event;

import org.springframework.context.ApplicationEvent;

/**
 * @author chenjianbo
 */
public class KafkaEvent<T extends KafkaObject> extends ApplicationEvent {

    public KafkaEvent(Object source) {
        super(source);
    }

    public T kafkaObject() {
        return (T) getSource();
    }
}

package com.sinsz.im.common;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.lang.reflect.UndeclaredThrowableException;
import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.util.UUID;

/**
 * 工具类
 * @author chenjianbo
 * @date 2017/9/26
 */
public class Util {

    private Util() {}

    /**
     * OTP密码算法
     */
    private static class OTP{
        private OTP() {}
        private volatile static OTP otp = new OTP();
        private final int[] DIGITS_POWER
                // 0 1 2 3 4 5 6 7 8
                = { 1, 10, 100, 1000, 10000, 100000, 1000000, 10000000, 100000000 };
        private byte[] hmac_sha(String crypto, byte[] keyBytes, byte[] text) {
            try {
                Mac hmac;
                hmac = Mac.getInstance(crypto);
                SecretKeySpec macKey = new SecretKeySpec(keyBytes, "RAW");
                hmac.init(macKey);
                return hmac.doFinal(text);
            } catch (GeneralSecurityException gse) {
                throw new UndeclaredThrowableException(gse);
            }
        }
        private byte[] hexStr2Bytes(String hex) {
            byte[] bArray = new BigInteger("10" + hex, 16).toByteArray();
            byte[] ret = new byte[bArray.length - 1];
            for (int i = 0; i < ret.length; i++) {
                ret[i] = bArray[i + 1];
            }
            return ret;
        }
        private String generateTOTP(
                String key,
                String time,
                String returnDigits,
                String crypto) {
            int codeDigits = Integer.decode(returnDigits);
            String result;
            while (time.length() < 16) {
                time = "0" + time;
            }
            byte[] msg = hexStr2Bytes(time);
            byte[] k = hexStr2Bytes(key);
            byte[] hash = hmac_sha(crypto, k, msg);
            int offset = hash[hash.length - 1] & 0xf;
            int binary = ((hash[offset] & 0x7f) << 24)
                    | ((hash[offset + 1] & 0xff) << 16)
                    | ((hash[offset + 2] & 0xff) << 8) | (hash[offset + 3] & 0xff);
            int otp = binary % DIGITS_POWER[codeDigits];
            result = Integer.toString(otp);
            while (result.length() < codeDigits) {
                result = "0" + result;
            }
            return result;
        }
        private static String generateTOTP(
                String key,
                String time,
                String returnDigits) {
            return otp.generateTOTP(key, time, returnDigits, "HmacSHA1");
        }
        private static String generateTOTP256(
                String key,
                String time,
                String returnDigits) {
            return otp.generateTOTP(key, time, returnDigits, "HmacSHA256");
        }
        private static String generateTOTP512(
                String key,
                String time,
                String returnDigits) {
            return otp.generateTOTP(key, time, returnDigits, "HmacSHA512");
        }
    }

    /**
     * 一次性密码调用
     * @return
     */
    public static String getOTP() {
        return OTP.generateTOTP512(
                UUID.randomUUID().toString().replaceAll("-",""),
                String.valueOf(System.nanoTime()),
                "8"
        );
    }

}

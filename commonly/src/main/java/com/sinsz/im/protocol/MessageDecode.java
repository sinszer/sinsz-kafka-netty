package com.sinsz.im.protocol;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

import java.util.List;

/**
 * 消息解码器
 */
public class MessageDecode extends ByteToMessageDecoder {
    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
        if (in == null) {
            ctx.close();
            return ;
        }
        if (in.readableBytes() < 6) {
            return ;
        }
        in.markReaderIndex();
        int dataLength = in.readInt();
        //因为包头预留了两个字节的大小，所以要预先读掉两个字节
        in.readByte();
        in.readByte();
        if (in.readableBytes() < dataLength) {
            in.resetReaderIndex();
            return;
        }
        byte[] body = new byte[dataLength];
        in.readBytes(body);
        Object object = SMessage.MessageBuf.parseFrom(body);
        out.add(object);
    }
}

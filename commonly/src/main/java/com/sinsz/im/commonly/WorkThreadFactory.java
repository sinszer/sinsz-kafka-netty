package com.sinsz.im.commonly;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 线程工厂定义
 */
public class WorkThreadFactory implements ThreadFactory {

    /*线程池编号*/
    private static AtomicInteger pool = new AtomicInteger(1);
    /*线程编号*/
    private static AtomicInteger thread = new AtomicInteger(1);

    private final ThreadGroup group;

    private String threadName;

    private boolean isDaemon;

    public WorkThreadFactory() {
        this("pool");
    }

    public WorkThreadFactory(String threadName) {
        this(threadName, false);
    }

    public WorkThreadFactory(String threadName, boolean isDaemon) {
        SecurityManager s = System.getSecurityManager();
        group = (s != null) ? s.getThreadGroup() : Thread.currentThread().getThreadGroup();
        this.threadName = threadName + "-" + String.format("%03d", pool.getAndIncrement()) + "-thread-";
        this.isDaemon = isDaemon;
    }

    @Override
    public Thread newThread(Runnable r) {
        Thread t = new Thread(group, r,threadName + String.format("%03d", thread.getAndIncrement()),0);
        t.setDaemon(isDaemon);
        if (t.getPriority() != Thread.NORM_PRIORITY) {
            t.setPriority(Thread.NORM_PRIORITY);
        }
        return t;
    }
}

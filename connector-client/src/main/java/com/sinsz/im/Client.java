package com.sinsz.im;

import com.sinsz.im.protocol.MessageDecode;
import com.sinsz.im.protocol.MessageEncode;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;

public class Client {

    private void server(String host, int port) {
        EventLoopGroup group = new NioEventLoopGroup();
        try {
            Bootstrap bootstrap = new Bootstrap();
            bootstrap.group(group)
                    .channel(NioSocketChannel.class)
                    .option(ChannelOption.SO_KEEPALIVE, true)
                    .remoteAddress(host, port)
                    .handler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        protected void initChannel(SocketChannel ch) throws Exception {
                            ChannelPipeline pipeline = ch.pipeline();
                            pipeline.addLast("decoder", new MessageDecode())
                                    .addLast("encoder", new MessageEncode())
                                    .addLast(new SinszChannelHandler());
                        }
                    });
            ChannelFuture future = bootstrap.connect(host, port).sync();
            future.channel().closeFuture().sync();
        } catch (Exception e) {
        } finally {
            group.shutdownGracefully();
        }
    }

    public static void main(String[] args) {
        // {"token":"4b21a9e8d083475c83277f202e877d81","userId":"59dc6f0e0a85d325a20658e8"}
        // {"token":"79a6d4fbeff84b8db92a554d5b756274","userId":"59ddc9f20a85d3307a95a5ff"}
        Constant.instance.from(
                "79a6d4fbeff84b8db92a554d5b756274",
                "59ddc9f20a85d3307a95a5ff",
                "陈健波2hao"
        );
        Constant.instance.to(
                "59dc6f0e0a85d325a20658e8",
                ""
        );
        new Client().server("127.0.0.1", 8551);
    }

}

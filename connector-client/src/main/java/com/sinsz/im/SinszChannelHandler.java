package com.sinsz.im;

import com.sinsz.im.protocol.SMessage;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

/**
 * @author chenjianbo
 */
public class SinszChannelHandler extends ChannelInboundHandlerAdapter {

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        SMessage.MessageBuf buf = SMessage.MessageBuf.newBuilder()
                .setRequest(SMessage.MessageBuf.RequestCommandEnum.LOGIN)
                .setToken(Constant.instance.getToken())
                .build();
        ctx.channel().writeAndFlush(buf);
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        SMessage.MessageBuf buf = (SMessage.MessageBuf) msg;
        System.out.println(
                "\n" + buf.getRequest() + " \n<> "
                        + buf.getStatus() + "<> "
                        + buf.getStatus().getMessage() + "\n<> "
                        + buf.getMessage() + "<> "
                        + buf.getMessage().getBody() + "\n"
        );
        //登录回执->发送消息
        if (buf.getRequest().equals(SMessage.MessageBuf.RequestCommandEnum.RESPLOGIN)
                && buf.getStatus().getState()) {
            Constant.instance.setTicket(buf.getStatus().getTicket());
            Constant.instance.setSequence(buf.getStatus().getSequence());

            //群聊时，不要设置toUserId, 请设置toGroupId
            SMessage.MessageBuf buf1 = SMessage.MessageBuf.newBuilder()
                    .setRequest(SMessage.MessageBuf.RequestCommandEnum.SENDINFO)
                    .setTicket(Constant.instance.getTicket())
                    .setSequence(Constant.instance.getSequence())
                    .setSessionId(Constant.instance.getSessionId())
                    .setMessage(
                            SMessage.MessageBuf.Message.newBuilder()
                            .setMessageEnum(SMessage.MessageBuf.Message.MessageEnum.MESSAGE)
                            .setChildMessageEnum(SMessage.MessageBuf.Message.ChildMessageEnum.SINGLE)
                            .setChatEnum(SMessage.MessageBuf.Message.ChatEnum.EMOJI)
                            .setFromUserId(Constant.instance.getFromUserId())
                            .setFromUserName(Constant.instance.getFromUserName())
                            .setToUserId(Constant.instance.getToUserId())
                            .setToGroupId(Constant.instance.getToGroupId())
                            .setBody("/v/v")
                            .setClientTime(System.currentTimeMillis())
                    )
                    .build();
            ctx.channel().writeAndFlush(buf1);
        }
        //发送消息成功
        if (buf.getRequest().equals(SMessage.MessageBuf.RequestCommandEnum.RESPSENDINFO)
                && buf.getStatus().getState()) {
            Constant.instance.setSequence(buf.getStatus().getSequence());
            Constant.instance.setSessionId(buf.getStatus().getSessionId());
        }
        //响应去拉取
        if (buf.getRequest().equals(SMessage.MessageBuf.RequestCommandEnum.NOTICE)
                && buf.getStatus().getState()) {
            SMessage.MessageBuf buf2 = SMessage.MessageBuf.newBuilder()
                    .setRequest(SMessage.MessageBuf.RequestCommandEnum.RESPNOTICE)
                    .setTicket(Constant.instance.getTicket())
                    .setStatus(
                            SMessage.MessageBuf.Status.newBuilder()
                            .setState(true)
                            .setMessage("请求下发未读消息")
                    ).build();
            ctx.channel().writeAndFlush(buf2);
        }
        //响应接收到的消息
        if (buf.getRequest().equals(SMessage.MessageBuf.RequestCommandEnum.RECEIVE)
                && buf.getStatus().getState()) {
            SMessage.MessageBuf buf3 = SMessage.MessageBuf.newBuilder()
                    .setRequest(SMessage.MessageBuf.RequestCommandEnum.RESPRECEIVE)
                    .setTicket(Constant.instance.getTicket())
                    .setMessageId(buf.getMessage().getMessageId())
                    .setStatus(
                            SMessage.MessageBuf.Status.newBuilder()
                                    .setState(true)
                                    .setMessage("已经成功接收到消息了")
                    )
                    .build();
            ctx.channel().writeAndFlush(buf3);
        }
    }

    /**
     * 主动停止前
     * @param ctx
     * @throws Exception
     */
    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
    }

    /**
     * 异常执行时
     * @param ctx
     * @param cause
     * @throws Exception
     */
    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        System.out.println("()()()()()()()()()()()()");
        ctx.channel().close();
    }
}
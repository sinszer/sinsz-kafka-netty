package com.sinsz.im;

/**
 * @author chenjianbo
 * @date 2017/10/16
 */
public class Constant {

    public static Constant instance = new Constant();

    private String token = "";

    private String fromUserId = "";

    private String fromUserName = "";

    private String toUserId = "";

    private String toGroupId = "";

    private String sessionId = "";

    private String ticket = "";

    private long sequence = 0;

    public void from(String token, String fromUserId, String fromUserName) {
        setToken(token);
        setFromUserId(fromUserId);
        setFromUserName(fromUserName);
    }

    public void to(String toUser, String toGroup) {
        setToUserId(toUser);
        setToGroupId(toGroup);
    }

    private Constant(){}

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getFromUserId() {
        return fromUserId;
    }

    public void setFromUserId(String fromUserId) {
        this.fromUserId = fromUserId;
    }

    public String getToUserId() {
        return toUserId;
    }

    public void setToUserId(String toUserId) {
        this.toUserId = toUserId;
    }

    public String getToGroupId() {
        return toGroupId;
    }

    public void setToGroupId(String toGroupId) {
        this.toGroupId = toGroupId;
    }

    public long getSequence() {
        return sequence;
    }

    public void setSequence(long sequence) {
        this.sequence = sequence;
    }

    public String getTicket() {
        return ticket;
    }

    public void setTicket(String ticket) {
        this.ticket = ticket;
    }

    public String getFromUserName() {
        return fromUserName;
    }

    public void setFromUserName(String fromUserName) {
        this.fromUserName = fromUserName;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }
}

package com.sinsz.im.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "mysql_test")
public class MysqlTest {

    @Id
    private String id;

    @Column(name = "name", length = 50)
    private String name;

    @Column(name = "name_desc", length = 50)
    private String nameDesc;

    public MysqlTest() {
    }

    public MysqlTest(String id, String name, String nameDesc) {
        this.id = id;
        this.name = name;
        this.nameDesc = nameDesc;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNameDesc() {
        return nameDesc;
    }

    public void setNameDesc(String nameDesc) {
        this.nameDesc = nameDesc;
    }
}

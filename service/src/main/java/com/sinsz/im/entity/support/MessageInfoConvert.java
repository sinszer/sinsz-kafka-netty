package com.sinsz.im.entity.support;

import com.sinsz.im.entity.MessageInfo;
import com.sinsz.im.protocol.SMessage;

import java.util.List;

/**
 * 消息内容互相转换方法
 * @author chenjianbo
 * @date 2017/10/16
 */
public class MessageInfoConvert {

    private MessageInfoConvert() {}

    /**
     * protoBuf的消息内容转换为存储的消息内容
     * @param buf
     * @return
     */
    public static MessageInfo toMessageInfo(SMessage.MessageBuf buf) {
        SMessage.MessageBuf.Message message = buf.getMessage();
        MessageInfo info = new MessageInfo(
              message.getMessageEnum(),
                message.getChildMessageEnum(),
                message.getChatEnum(),
                message.getFromUserId(),
                message.getFromUserName(),
                message.getToUserId(),
                message.getToGroupId(),
                message.getTitle(),
                message.getSummary(),
                message.getBody(),
                message.getClientTime(),
                message.getServerTime(),
                message.getMessageId(),
                message.getAtList()
        );
        info.setSessionId(buf.getSessionId());
        return info;
    }

    /**
     * 将存储的消息内容转换为协议消息内容
     * @param info
     * @return
     */
    public static SMessage.MessageBuf toMessageBuf(MessageInfo info) {
        SMessage.MessageBuf buf = SMessage.MessageBuf.newBuilder()
                .setStatus(
                        SMessage.MessageBuf.Status.newBuilder()
                        .setSessionId(info.getSessionId())
                )
                .setMessage(
                        SMessage.MessageBuf.Message.newBuilder()
                        .setMessageEnum(info.getMessageEnum())
                        .setChildMessageEnum(info.getChildMessageEnum())
                        .setChatEnum(info.getChatEnum())
                        .setFromUserId(info.getFromUserId())
                        .setFromUserName(info.getFromUserName())
                        .setToUserId(info.getToUserId())
                        .setToGroupId(info.getToGroupId())
                        .setTitle(info.getTitle())
                        .setSummary(info.getSummary())
                        .setBody(info.getBody())
                        .setClientTime(info.getClientTime())
                        .setServerTime(info.getServerTime())
                        .setMessageId(info.getMessageId())
                        .build()
                ).build();
        List<SMessage.MessageBuf.Message.At> list = info.getAt();
        if (list != null && list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                buf = buf.toBuilder().setMessage(
                        buf.getMessage().toBuilder().setAt(i, list.get(i)).build()
                ).build();
            }
        }
        return buf;
    }

}
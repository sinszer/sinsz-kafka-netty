package com.sinsz.im.entity;

import com.sinsz.im.protocol.SMessage;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

/**
 * 消息类容存储
 * @author chenjianbo
 * @date 2017/10/12
 */
@Document
public class MessageInfo {

    @Id
    private String id;
    @Indexed(name = "_session_id")
    private String sessionId;
    private SMessage.MessageBuf.Message.MessageEnum messageEnum;
    private SMessage.MessageBuf.Message.ChildMessageEnum childMessageEnum;
    private SMessage.MessageBuf.Message.ChatEnum chatEnum;
    @Indexed(name = "_from_user_id")
    private String fromUserId;
    private String fromUserName;
    @Indexed(name = "_to_user_id")
    private String toUserId;
    @Indexed(name = "_to_group_id")
    private String toGroupId;
    private String title;
    private String summary;
    private String body;
    private long clientTime;
    private long serverTime;
    @Indexed(name = "_message_id")
    private long messageId;
    private List<SMessage.MessageBuf.Message.At> at;

    public MessageInfo() {
    }

    public MessageInfo(SMessage.MessageBuf.Message.MessageEnum messageEnum, SMessage.MessageBuf.Message
            .ChildMessageEnum childMessageEnum, SMessage.MessageBuf.Message.ChatEnum chatEnum, String fromUserId,
                       String fromUserName, String toUserId, String toGroupId, String title, String summary, String
                               body, long clientTime, long serverTime, long messageId, List<SMessage.MessageBuf
            .Message.At> at) {
        this.messageEnum = messageEnum;
        this.childMessageEnum = childMessageEnum;
        this.chatEnum = chatEnum;
        this.fromUserId = fromUserId;
        this.fromUserName = fromUserName;
        this.toUserId = toUserId;
        this.toGroupId = toGroupId;
        this.title = title;
        this.summary = summary;
        this.body = body;
        this.clientTime = clientTime;
        this.serverTime = serverTime;
        this.messageId = messageId;
        this.at = at;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public SMessage.MessageBuf.Message.MessageEnum getMessageEnum() {
        return messageEnum;
    }

    public void setMessageEnum(SMessage.MessageBuf.Message.MessageEnum messageEnum) {
        this.messageEnum = messageEnum;
    }

    public SMessage.MessageBuf.Message.ChildMessageEnum getChildMessageEnum() {
        return childMessageEnum;
    }

    public void setChildMessageEnum(SMessage.MessageBuf.Message.ChildMessageEnum childMessageEnum) {
        this.childMessageEnum = childMessageEnum;
    }

    public SMessage.MessageBuf.Message.ChatEnum getChatEnum() {
        return chatEnum;
    }

    public void setChatEnum(SMessage.MessageBuf.Message.ChatEnum chatEnum) {
        this.chatEnum = chatEnum;
    }

    public String getFromUserId() {
        return fromUserId;
    }

    public void setFromUserId(String fromUserId) {
        this.fromUserId = fromUserId;
    }

    public String getFromUserName() {
        return fromUserName;
    }

    public void setFromUserName(String fromUserName) {
        this.fromUserName = fromUserName;
    }

    public String getToUserId() {
        return toUserId;
    }

    public void setToUserId(String toUserId) {
        this.toUserId = toUserId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public long getClientTime() {
        return clientTime;
    }

    public void setClientTime(long clientTime) {
        this.clientTime = clientTime;
    }

    public long getServerTime() {
        return serverTime;
    }

    public void setServerTime(long serverTime) {
        this.serverTime = serverTime;
    }

    public long getMessageId() {
        return messageId;
    }

    public void setMessageId(long messageId) {
        this.messageId = messageId;
    }

    public List<SMessage.MessageBuf.Message.At> getAt() {
        return at;
    }

    public void setAt(List<SMessage.MessageBuf.Message.At> at) {
        this.at = at;
    }

    public String getToGroupId() {
        return toGroupId;
    }

    public void setToGroupId(String toGroupId) {
        this.toGroupId = toGroupId;
    }
}

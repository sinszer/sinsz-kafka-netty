package com.sinsz.im.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * 用户
 * @author chenjianbo
 * @date 2017/10/10
 */
@Document
public class User {

    @Id
    private String id;

    private String user;

    private String passwd;

    private String name;

    public User() {
    }

    public User(String user, String passwd, String name) {
        this.user = user;
        this.passwd = passwd;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPasswd() {
        return passwd;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
package com.sinsz.im.aop;

import com.sinsz.im.exception.IError;
import com.sinsz.im.exception.SZError;
import com.sinsz.im.exception.SZException;
import com.sinsz.im.result.Result;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import javax.servlet.http.HttpServletRequest;
import java.sql.SQLException;

@Component
@Aspect
@Order(1)
public class ThrowableErrorAop {

    private static final Logger logger = LoggerFactory.getLogger(ThrowableErrorAop.class);

    @Autowired
    private HttpServletRequest request;

    /**
     * 拦截切入点
     */
    @Pointcut("execution(* com.sinsz.im..*Controller.*(..))")
    public void web(){}

    /**
     * 1、rest满足切入条件
     */
    @Pointcut("@within(org.springframework.web.bind.annotation.RestController)")
    public void rest() {}

    /**
     * 2.1、传统方式类注解满足切入条件
     */
    @Pointcut("@within(org.springframework.stereotype.Controller)")
    public void controller() {}

    /**
     * 2.2、传统方式方法注解满足切入条件
     */
    @Pointcut("@annotation(org.springframework.web.bind.annotation.ResponseBody)")
    public void responseBody() {}

    @Around("(web() && rest()) " +
            "|| (web() && controller() && responseBody() )")
    public Object around(ProceedingJoinPoint joinPoint) {
        Object retVal;
        long starterTime = System.currentTimeMillis();
        try {
            retVal= joinPoint.proceed();
            if (logger.isDebugEnabled()) {
                logger.debug("地址:{} ; 路径:{} ; 执行方法:{} ; 耗时:{} 毫秒.",
                        request.getRemoteHost(),
                        request.getRequestURI(),
                        joinPoint.getSignature().toShortString(),
                        System.currentTimeMillis() - starterTime);
            }
        } catch (Throwable throwable) {
            IError error;
            if (throwable instanceof SZException) {
                SZException ae = (SZException) throwable;
                error = ae.getError();
            } else if (throwable.getCause() instanceof SQLException) {
                error = SZError.SYSTEM_SQL_ERR;
            } else {
                error = SZError.SYSTEM_ERR;
            }
            retVal = Result.fail(error, throwable);
            if (logger.isErrorEnabled()) {
                logger.error("地址:{} ; 路径:{} ; 执行方法:{} ; 耗时:{} 毫秒. 异常消息: {}",
                        request.getRemoteHost(),
                        request.getRequestURI(),
                        joinPoint.getSignature().toShortString(),
                        System.currentTimeMillis() - starterTime,
                        throwable.getMessage());
            }
            if (logger.isDebugEnabled()) {
                throwable.printStackTrace(System.out);
            }
        }
        return retVal;
    }

}
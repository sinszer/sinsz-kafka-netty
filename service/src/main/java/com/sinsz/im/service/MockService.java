package com.sinsz.im.service;

import com.sinsz.im.enums.AppEnum;
import com.sinsz.im.service.dto.LoginResultDto;

public interface MockService {

    void reg(
            String user,
            String passwd,
            String name
    );

    LoginResultDto login(
            String user,
            String passwd,
            AppEnum app,
            String deviceToken
    );
}
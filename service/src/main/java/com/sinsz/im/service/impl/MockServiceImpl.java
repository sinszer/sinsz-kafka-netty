package com.sinsz.im.service.impl;

import com.sinsz.im.entity.User;
import com.sinsz.im.enums.AppEnum;
import com.sinsz.im.exception.IError;
import com.sinsz.im.exception.SZException;
import com.sinsz.im.repository.MultiTokenRepository;
import com.sinsz.im.repository.TokenRepository;
import com.sinsz.im.repository.mongodb.UserRepository;
import com.sinsz.im.repository.support.Token;
import com.sinsz.im.service.MockService;
import com.sinsz.im.service.dto.LoginResultDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import java.util.UUID;

@Service("mockService")
public class MockServiceImpl implements MockService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TokenRepository tokenRepository;

    @Autowired
    private MultiTokenRepository multiTokenRepository;

    private void err001() {
        throw new SZException(new IError() {
            @Override
            public String getCode() {
                return "user-001";
            }

            @Override
            public String getMessage() {
                return "参数不能为空";
            }
        });
    }

    @Override
    public void reg(String user, String passwd, String name) {
        if (StringUtils.isEmpty(user)
                || StringUtils.isEmpty(passwd)
                || StringUtils.isEmpty(name)) {
            err001();
        }
        User u = userRepository.findByUser(user);
        if (u != null) {
            throw new SZException(new IError() {
                @Override
                public String getCode() {
                    return "user-002";
                }

                @Override
                public String getMessage() {
                    return "用户已存在";
                }
            });
        }
        u = new User(user, passwd, name);
        userRepository.save(u);
    }

    @Override
    public LoginResultDto login(String user, String passwd, AppEnum app, String deviceToken) {
        if (StringUtils.isEmpty(user)
                || StringUtils.isEmpty(passwd)
                || ObjectUtils.isEmpty(app)) {
            err001();
        }
        User u = userRepository.findByUserAndPasswd(user, passwd);
        if (u == null) {
            throw new SZException(new IError() {
                @Override
                public String getCode() {
                    return "user-003";
                }

                @Override
                public String getMessage() {
                    return "账号或密码错误";
                }
            });
        }
        // 创建token与保持信息映射
        Token token = new Token(
                u.getId(),
                u.getName(),
                System.currentTimeMillis(),
                deviceToken,
                app
        );
        String uuid = UUID.randomUUID().toString().toLowerCase().replaceAll("-", "");
        tokenRepository.insert(uuid, token);
        multiTokenRepository.insert(u.getId(), uuid);
        // 创建返回值
        LoginResultDto dto = new LoginResultDto();
        dto.setToken(uuid);
        dto.setUserId(u.getId());
        return dto;
    }


}
package com.sinsz.im.repository.mongodb;

import com.sinsz.im.entity.MessageInfo;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface MessageInfoRepository extends MongoRepository<MessageInfo, String> {
}

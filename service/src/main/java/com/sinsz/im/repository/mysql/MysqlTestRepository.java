package com.sinsz.im.repository.mysql;

import com.sinsz.im.entity.MysqlTest;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MysqlTestRepository extends JpaRepository<MysqlTest, String> {
}

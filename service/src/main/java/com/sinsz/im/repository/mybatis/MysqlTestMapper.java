package com.sinsz.im.repository.mybatis;

import com.sinsz.im.entity.MysqlTest;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface MysqlTestMapper {

    @Insert("insert into mysql_test(id,name,name_desc) values(#{id},#{name},#{nameDesc})")
    void insert(@Param("id") String id, @Param("name") String name, @Param("nameDesc") String nameDesc);

    @Select("select * from mysql_test")
    List<MysqlTest> findAll();
}
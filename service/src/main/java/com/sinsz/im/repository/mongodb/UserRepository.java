package com.sinsz.im.repository.mongodb;

import com.sinsz.im.entity.User;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface UserRepository extends MongoRepository<User, String> {

    User findByUser(String user);

    User findByUserAndPasswd(String user, String passwd);
}

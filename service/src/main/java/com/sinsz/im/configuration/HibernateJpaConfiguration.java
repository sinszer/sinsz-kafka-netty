package com.sinsz.im.configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories(basePackages = {"com.sinsz.im.repository.mysql"})
@Order(4)
public class HibernateJpaConfiguration {

    private static final Logger logger = LoggerFactory.getLogger(HibernateJpaConfiguration.class);

    public HibernateJpaConfiguration() {
        logger.info(">>>>>>>>>>>>>>>>>>>> HibernateJpa扫描 >>>>>>>>>>>>>>>>>>");
    }

}
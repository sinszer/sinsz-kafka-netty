package com.sinsz.im.configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@Configuration
@EnableMongoRepositories(basePackages = {"com.sinsz.im.repository.mongodb"})
@Order(5)
public class MongodbJpaConfiguration {

    private static final Logger logger = LoggerFactory.getLogger(MongodbJpaConfiguration.class);

    public MongodbJpaConfiguration() {
        logger.info(">>>>>>>>>>>>>>>>>>>> MongoDbJpa扫描 >>>>>>>>>>>>>>>>>>");
    }
}
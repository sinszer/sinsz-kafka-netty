package com.sinsz.im.configuration;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;

import javax.annotation.Resource;
import javax.sql.DataSource;

@Configuration
@MapperScan("com.sinsz.im.repository.mybatis")
@Order(6)
public class MybatisMapperConfiguration {

    private static final Logger logger = LoggerFactory.getLogger(MybatisMapperConfiguration.class);

    public MybatisMapperConfiguration() {
        logger.info(">>>>>>>>>>>>>>>>>>>> Mybatis初始化 >>>>>>>>>>>>>>>>>>");
    }

    @Bean("entityConfiguration")
    public org.apache.ibatis.session.Configuration entityConfiguration() {
        org.apache.ibatis.session.Configuration conf = new org.apache.ibatis.session.Configuration();
        conf.setMapUnderscoreToCamelCase(true);
        return conf;
    }

    @Resource(name = "entityConfiguration")
    private org.apache.ibatis.session.Configuration entityConfiguration;

    @Autowired(required = false)
    private DataSource dataSource;

    @Bean(name = "sqlSessionFactory")
    public SqlSessionFactoryBean createSqlSessionFactoryBean() throws Exception {
        SqlSessionFactoryBean sqlSessionFactoryBean = new SqlSessionFactoryBean();
        sqlSessionFactoryBean.setDataSource(dataSource);
        sqlSessionFactoryBean.setTypeAliasesPackage(
                "com.sinsz.im.entity"
        );
        sqlSessionFactoryBean.setConfiguration(entityConfiguration);
        return sqlSessionFactoryBean;
    }

    @Bean
    public SqlSessionTemplate sqlSessionTemplate(SqlSessionFactory sqlSessionFactory) {
        return new SqlSessionTemplate(sqlSessionFactory);
    }

}
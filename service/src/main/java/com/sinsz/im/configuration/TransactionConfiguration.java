package com.sinsz.im.configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.TransactionManagementConfigurer;
import javax.annotation.Resource;
import javax.sql.DataSource;

@Configuration
@EnableTransactionManagement
@Order(7)
public class TransactionConfiguration implements TransactionManagementConfigurer {

    private static final Logger logger = LoggerFactory.getLogger(TransactionConfiguration.class);

    public TransactionConfiguration() {
        logger.info(">>>>>>>>>>>>>>>>>>>> 事务管理初始化 >>>>>>>>>>>>>>>>>>");
    }

    @Resource(name = "transactionManager")
    private PlatformTransactionManager transactionManager;

    @Autowired(required = false)
    private DataSource dataSource;

    /**
     * Jpa事务管理 支持 Mybatis事务控制
     * @return
     */
    @Bean
    public PlatformTransactionManager transactionManager() {
        JpaTransactionManager manager = new JpaTransactionManager();
        manager.setDataSource(dataSource);
        manager.setRollbackOnCommitFailure(true);
        return manager;
    }

    /**
     * Mybatis 备用事务管理器
     * @return
     */
    @Bean
    public PlatformTransactionManager mybatisTxManager() {
        DataSourceTransactionManager manager = new DataSourceTransactionManager();
        manager.setDataSource(dataSource);
        manager.setRollbackOnCommitFailure(true);
        return manager;
    }

    /**
     * 设置自动事务管理器
     * @return
     */
    @Override
    public PlatformTransactionManager annotationDrivenTransactionManager() {
        return transactionManager;
    }

}
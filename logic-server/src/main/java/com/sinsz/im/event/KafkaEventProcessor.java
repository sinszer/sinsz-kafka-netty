package com.sinsz.im.event;

import com.sinsz.im.producer.SendMessageProducer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.core.Ordered;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

/**
 * 重发监听
 * @author chenjianbo
 */
@Component
public class KafkaEventProcessor implements ApplicationListener<KafkaEvent<KafkaObject>>, Ordered {

    private static final Logger logger = LoggerFactory.getLogger(KafkaEventProcessor.class);

    @Autowired
    private SendMessageProducer sendMessageProducer;

    @Async
    @Override
    public void onApplicationEvent(KafkaEvent<KafkaObject> event) {
        if (logger.isDebugEnabled()) {
            logger.debug("开始重发消息。。。。。。");
        }
        KafkaObject kafkaObject = event.kafkaObject();
        sendMessageProducer.listenerRetry(
                kafkaObject.getTopic(),
                kafkaObject.getPartition(),
                kafkaObject.getKey(),
                kafkaObject.getValue()
        );
    }

    @Override
    public int getOrder() {
        return 1;
    }
}

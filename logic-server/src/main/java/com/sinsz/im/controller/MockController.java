package com.sinsz.im.controller;

import com.sinsz.im.enums.AppEnum;
import com.sinsz.im.result.Result;
import com.sinsz.im.service.MockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 模拟接口
 * @author chenjianbo
 * @date 2017/10/10
 */
@RestController
@RequestMapping("/mock")
public class MockController {

    @Autowired
    private MockService mockService;

    /**
     * 模拟注册
     * @param user
     * @param passwd
     * @param name
     * @return
     */
    @RequestMapping("/reg")
    public Result mockRegister(
            @RequestParam String user,
            @RequestParam String passwd,
            @RequestParam String name
    ) {
        mockService.reg(user, passwd, name);
        return Result.success();
    }

    /**
     * 模拟登录
     * @param user
     * @param passwd
     * @param app
     * @param deviceToken
     * @return
     */
    @RequestMapping("/login")
    public Result mockLogin(
            @RequestParam String user,
            @RequestParam String passwd,
            @RequestParam AppEnum app,
            @RequestParam String deviceToken
    ) {
        return Result.success(
                mockService.login(user, passwd, app, deviceToken)
        );
    }

}
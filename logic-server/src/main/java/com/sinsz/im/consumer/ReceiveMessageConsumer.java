package com.sinsz.im.consumer;

import com.sinsz.im.common.Util;
import com.sinsz.im.entity.support.MessageInfoConvert;
import com.sinsz.im.kafka.KafkaCommand;
import com.sinsz.im.kafka.KafkaMessage;
import com.sinsz.im.producer.SendMessageProducer;
import com.sinsz.im.protocol.SMessage;
import com.sinsz.im.repository.*;
import com.sinsz.im.repository.mongodb.MessageInfoRepository;
import com.sinsz.im.repository.support.GroupInfo;
import com.sinsz.im.repository.support.Token;
import com.sinsz.im.repository.support.UnRead;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.listener.MessageListener;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.stream.Collectors;


/**
 * netty对用户发送上来消息的消费
 * @author chenjianbo
 * @date 2017/9/27
 */
@Component
public class ReceiveMessageConsumer implements MessageListener<String, KafkaMessage> {

    private static final Logger logger = LoggerFactory.getLogger(ReceiveMessageConsumer.class);

    @Autowired
    private SendMessageProducer sendMessageProducer;

    @Autowired
    private TokenRepository tokenRepository;

    @Autowired
    private MultiTokenRepository multiTokenRepository;

    @Autowired
    private SequenceRepository sequenceRepository;

    @Autowired
    private ExecutorService executorService;

    @Autowired
    private MessageIdRepository messageIdRepository;

    @Autowired
    private SessionChatRepository sessionChatRepository;

    @Autowired
    private GroupInfoRepository groupInfoRepository;

    @Autowired
    private MessageInfoRepository messageInfoRepository;

    @Autowired
    private UnReadRepository unReadRepository;

    /**
     * 监听用户发送上来的消息内容
     * 并作相应的逻辑处理
     * @param data
     */
    @Override
    public void onMessage(ConsumerRecord<String, KafkaMessage> data) {
        KafkaMessage message = data.value();
        switch (message.getMessageBuf().getRequest()) {
            case LOGIN:
                responseLogin(
                        message.getTopic(),
                        message.getToken(),
                        message
                );
                break;
            case SENDINFO:
                responseSendInfo(message);
                break;
            case RESPNOTICE:
                sendMessageToReceive(message);
                break;
            case RESPRECEIVE:
                responseFromReceive(message);
                break;
            default:
                if (logger.isDebugEnabled()) {
                    logger.debug("该命令【{}】尚未实现...", message.getMessageBuf().getRequest());
                }
        }
    }



    /**
     * 登录验证
     * @param topic
     * @param token
     */
    private void responseLogin(String topic, String token, KafkaMessage message) {
        //【第一步】
        // 处理回执，用户尚未登录处理
        Token tk = tokenRepository.get(token);
        if (tk == null) {
            //请先登录业务系统
            SMessage.MessageBuf result = SMessage.MessageBuf
                    .newBuilder()
                    .setRequest(SMessage.MessageBuf.RequestCommandEnum.RESPLOGIN)
                    .setStatus(
                            SMessage.MessageBuf.Status.newBuilder()
                            .setState(false)
                            .setServerTime(System.currentTimeMillis())
                            .setMessage("请先登录业务系统").build()
                    ).build();
            message.setCommand(KafkaCommand.MESSAGE);
            message.setMessageTime(System.currentTimeMillis());
            message.setMessageBuf(result);
            message.setBody("请先登录业务系统");
            sendMessageProducer.send(topic, message);
            return;
        }
        //【第二步】
        // 更新topic到token中
        tk.setTopic(topic);
        // 更新channelId到token中
        tk.setChannelId(message.getChannelId());
        tokenRepository.insert(token, tk);
        // 判断该用户是否有sequence编号，有则返回 N+1，无则创建并返回1
        long sequence = 1;
        if (sequenceRepository.exist(tk.getUserId())) {
            sequence = sequenceRepository.get(tk.getUserId()) + 1;
        } else {
            sequenceRepository.insert(tk.getUserId(), 0);
        }
        // 更新回执kafkaMessage
        SMessage.MessageBuf result = SMessage.MessageBuf
                .newBuilder()
                .setRequest(SMessage.MessageBuf.RequestCommandEnum.RESPLOGIN)
                .setStatus(
                        SMessage.MessageBuf.Status.newBuilder()
                        .setState(true)
                        .setServerTime(System.currentTimeMillis())
                        .setMessage("登录成功")
                        .setTicket(Util.getOTP())
                        .setSequence(sequence)
                        .build()
                ).build();
        message.setCommand(KafkaCommand.MESSAGE);
        message.setMessageTime(System.currentTimeMillis());
        message.setBody("登录成功");
        message.setUserId(tk.getUserId());
        message.setMessageBuf(result);
        sendMessageProducer.send(topic, message);
        //【第三步】
        // 异步踢人
        executorService.execute(() -> {
            Set<Object> sets = multiTokenRepository.getAll(tk.getUserId());
            if (sets != null) {
                sets.iterator().forEachRemaining(str -> {
                    String tokenStr = String.valueOf(str);
                    //排除当前登录
                    if (!tokenStr.equals(token)) {
                        //判断token有不有队列，有队列发送登出命令
                        Token historyToken = tokenRepository.get(tokenStr);
                        if (historyToken != null
                                && !StringUtils.isEmpty(historyToken.getTopic())) {
                            //您已在其他设备登录
                            SMessage.MessageBuf rt = SMessage.MessageBuf
                                    .newBuilder()
                                    .setRequest(SMessage.MessageBuf.RequestCommandEnum.RESPLOGIN)
                                    .setStatus(
                                            SMessage.MessageBuf.Status.newBuilder()
                                                    .setState(false)
                                                    .setServerTime(System.currentTimeMillis())
                                                    .setMessage("您已在其他设备登录").build()
                                    ).build();
                            KafkaMessage msg = new KafkaMessage();
                            msg.setTopic(historyToken.getTopic());
                            msg.setCommand(KafkaCommand.MESSAGE);
                            msg.setMessageTime(System.currentTimeMillis());
                            msg.setMessageBuf(rt);
                            msg.setBody("您已在其他设备登录");
                            msg.setUserId(historyToken.getUserId());
                            msg.setChannelId(historyToken.getChannelId());
                            sendMessageProducer.send(historyToken.getTopic(), msg);
                        }
                        //删除掉历史记录
                        tokenRepository.delete(tokenStr);
                        multiTokenRepository.delete(tk.getUserId(), tokenStr);
                    }
                });
            }
        });
        //【第四步】
        // 读取未读并（直接）下发到客户端
        executorService.execute(() -> {
            List<UnRead> list = unReadRepository.getAll(tk.getUserId());
            if (list != null && list.size() > 0) {
                sendReceiveInfo(tk.getUserId(), list.size());
            }
        });
    }

    /**
     * 回执发送消息相关处理
     * @param message
     */
    private void responseSendInfo(KafkaMessage message) {
        String topic = message.getTopic();
        //更新聊天记录，为聊天记录添加全局消息编号
        SMessage.MessageBuf buf = message.getMessageBuf().toBuilder()
                .setMessage(
                        message.getMessageBuf().getMessage().toBuilder()
                        .setServerTime(System.currentTimeMillis())
                        .setMessageId(messageIdRepository.get()).build()
                )
                .build();
        //验证消息序列编号
        long sequenceId = sequenceRepository.get(message.getUserId());
        if (buf.getSequence() < sequenceId) {
            sendInfoFailResult(message, "不是最新的消息序列号");
            return;
        }
        /**
         * 单聊时：SINGLE
         * A与B创建会话，并将A与B添加到会话redis中；
         * 当A或B邀请C进入会话时，将C加入到会话redis中，同时会主动向C下发一条邀请信息，C这时在该会话中聊天则带上会话ID进行聊天（类型 SINGLE）；
         * 如果C不带上会话ID和B聊天，则会以C+B创建一个新的会话；
         *
         * @@@ 成员退出聊天只剩1个时，业务系统需要吧该会话删掉
         *
         * 群聊时：GROUP
         * 以toGroupId为键创建会话，每次有人向服务器发送消息时，需要校验会话中的成员与群组成员的差异性。
         *
         * @@@ 成员退出聊天只剩1个时，业务系统需要吧该会话删掉
         *
         */
        String sessionId = buf.getSessionId();
        switch (buf.getMessage().getChildMessageEnum()) {
            case SINGLE:
                String from = buf.getMessage().getFromUserId();
                String to = buf.getMessage().getToUserId();
                if (StringUtils.isEmpty(sessionId)) {
                    int exist = sessionChatRepository.exist(from, to);
                    if (exist == 0) {
                        sessionChatRepository.insert(from, to, 0, from);
                        sessionChatRepository.insert(from, to, 0, to);
                        sessionId = from + "@" + to;
                    } else if (exist == 1) {
                        sessionId = from + "@" + to;
                    } else {
                        sessionId = to + "@" + from;
                    }
                    buf = buf.toBuilder()
                            .setSessionId(sessionId).build();
                }
                //获取该会话的所有成员
                List<String> mbs = sessionChatRepository.getAll(
                        from, to,
                        sessionChatRepository.exist(from, to));
                if (mbs != null && mbs.size() > 1) {
                    sendInfoToTopic(topic, message, mbs, buf);
                } else {
                    //当会话剩余1个人在里面时，将删除掉会话
                    sessionChatRepository.delete(from, to, sessionChatRepository.exist(from, to));
                    // 回执会话不存在
                    sendInfoFailResult(message, "聊天会话已销毁");
                }
                break;
            case GROUP:
                String groupId = buf.getMessage().getToGroupId();
                if (StringUtils.isEmpty(sessionId)) {
                    int exist = sessionChatRepository.exist(null, groupId);
                    if (exist == 0) {
                        //1.读取群列表
                        List<GroupInfo> groupInfos = groupInfoRepository.getAll(groupId);
                        //2.循环将群成员加入到会话中
                        groupInfos.forEach(g -> {
                            sessionChatRepository.insert(null, groupId, 0,
                                    g.getUserId());
                        });
                    } else {
                        //1.读取会话列表中的所有成员
                        List<String> members = sessionChatRepository.getAll(null, groupId, 1);
                        //2.读取群中的成员
                        List<String> groupInfos = groupInfoRepository.getAll(groupId)
                                .stream().map(GroupInfo::getUserId).collect(Collectors.toList());
                        //3.合并集合求出差异，群成员新增的就加入，群成员删除的就祛除
                        List<String> deletes = members.stream()
                                .filter(str -> !groupInfos.contains(str))
                                .collect(Collectors.toList());
                        //4.更新群成员到redis会话中
                        groupInfos.forEach(g -> sessionChatRepository.insert(null, groupId, 1,g));
                        //5.删除已经删掉的成员
                        deletes.forEach(g -> sessionChatRepository.delete(null, groupId, 1, g));
                    }
                    //设置会话ID
                    sessionId = "@" + groupId;
                    buf = buf.toBuilder()
                            .setSessionId(sessionId).build();
                }
                //获取最新的全部成员
                List<String> groups = sessionChatRepository.getAll(null, groupId, 1);
                if (groups != null && groups.size() > 1) {
                    sendInfoToTopic(topic, message, groups, buf);
                } else {
                    //当会话剩余1个人在里面时，将删除掉会话
                    sessionChatRepository.delete(null, groupId, 1);
                    // 回执会话不存在
                    sendInfoFailResult(message, "聊天会话已销毁");
                }
                break;
            case EVENT:
            case PUSH:
            default:
                if (logger.isDebugEnabled()) {
                    logger.debug("该命令暂未实现......");
                }
                sendInfoFailResult(message, "暂无该聊天类型");
        }
    }

    /**
     * 发送消息失败回执
     * @param message
     * @param errMessage
     */
    private void sendInfoFailResult(KafkaMessage message, String errMessage) {
        SMessage.MessageBuf rlt = SMessage.MessageBuf.newBuilder()
                .setRequest(SMessage.MessageBuf.RequestCommandEnum.RESPSENDINFO)
                .setStatus(
                        SMessage.MessageBuf.Status.newBuilder()
                                .setState(false)
                                .setMessage(errMessage)
                                .setServerTime(System.currentTimeMillis())
                                .build()
                ).build();
        message.setCommand(KafkaCommand.MESSAGE);
        message.setMessageTime(System.currentTimeMillis());
        message.setMessageBuf(rlt);
        message.setBody(errMessage);
        sendMessageProducer.send(message.getTopic(), message);
    }

    /**
     * 发送接收信息通知命令
     * @param userId
     * @param unReadNum
     */
    private void sendReceiveInfo(String userId, int unReadNum) {
        //查询其他人的所有token
        List<String> tokens = multiTokenRepository.getAll(userId).stream()
                .map(o -> (String)o).collect(Collectors.toList());
        if (tokens != null) {
            //查询登录键
            tokens.forEach(uuid -> {
                Token token = tokenRepository.get(uuid);
                if (token != null
                        && !StringUtils.isEmpty(token.getTopic())
                        && !StringUtils.isEmpty(token.getChannelId())) {
                    //发送通知消息
                    SMessage.MessageBuf result = SMessage.MessageBuf.newBuilder()
                            .setRequest(SMessage.MessageBuf.RequestCommandEnum.NOTICE)
                            .setStatus(
                                    SMessage.MessageBuf.Status.newBuilder()
                                    .setState(true)
                                    .setMessage("您有 " + unReadNum + " 条新消息")
                                    .setServerTime(System.currentTimeMillis())
                                    .build()
                            ).build();
                    KafkaMessage msg = new KafkaMessage();
                    msg.setTopic(token.getTopic());
                    msg.setCommand(KafkaCommand.MESSAGE);
                    msg.setMessageTime(System.currentTimeMillis());
                    msg.setMessageBuf(result);
                    msg.setBody("您有 " + unReadNum + " 条新消息");
                    msg.setUserId(token.getUserId());
                    msg.setChannelId(token.getChannelId());
                    sendMessageProducer.send(token.getTopic(), msg);
                }
            });
        }
    }

    /**
     * 公共的消息发送
     * <p>
     *     对消息的处理;
     *     为成员集合中的用户存储未读消息
     * </p>
     * @param topic
     * @param message
     * @param members
     * @param buf
     */
    private void sendInfoToTopic(String topic, KafkaMessage message, List<String> members,
                                 SMessage.MessageBuf buf) {
        String fromUserId = buf.getMessage().getFromUserId();
        //1.同步存储未读消息到redis；
        members.forEach(u -> {
            if (!u.equals(fromUserId)) {
                UnRead unRead = new UnRead(buf);
                unReadRepository.insert(u, unRead);
            }
        });
        //2.异步存储消息到mongodb
        executorService.execute(() -> messageInfoRepository.save(
                    MessageInfoConvert.toMessageInfo(buf)
            ));
        //3.异步发送receive给每一个topic
        executorService.execute(() -> members.forEach(member -> {
            //排除自己
            if (!member.equals(fromUserId)) {
                sendReceiveInfo(member, 1);
            }
        }));
        //4.最后sequenceId需要+1并回执成功
        sequenceRepository.insert(fromUserId, buf.getSequence());
        long sequence = buf.getSequence() + 1;
        SMessage.MessageBuf result = SMessage.MessageBuf.newBuilder()
                .setRequest(SMessage.MessageBuf.RequestCommandEnum.RESPSENDINFO)
                .setStatus(
                        SMessage.MessageBuf.Status.newBuilder()
                        .setState(true)
                        .setMessage("发送成功")
                        .setServerTime(System.currentTimeMillis())
                        .setSequence(sequence)
                        .setSessionId(buf.getSessionId())
                        .build()
                ).build();
        message.setCommand(KafkaCommand.MESSAGE);
        message.setBody("发送成功");
        message.setMessageTime(System.currentTimeMillis());
        message.setMessageBuf(result);
        sendMessageProducer.send(topic, message);
    }

    /**
     * 收到队列消息，
     * 将消息发送给用户
     * @param message
     */
    private void sendMessageToReceive(KafkaMessage message) {
        String userId = message.getUserId();
        String topic = message.getTopic();
        //获取未读消息
        if (unReadRepository.exist(userId)) {
            List<UnRead> unReads = unReadRepository.getAll(userId);
            if (unReads != null) {
                unReads.sort((u1, u2) -> {
                    long m1 = u1.getMessageBuf().getMessage().getMessageId();
                    long m2 = u2.getMessageBuf().getMessage().getMessageId();
                    return Long.compare(m1, m2);
                });
                unReads.forEach(unRead -> {
                    SMessage.MessageBuf buf = unRead.getMessageBuf();
                    SMessage.MessageBuf result = SMessage.MessageBuf.newBuilder()
                            .setRequest(SMessage.MessageBuf.RequestCommandEnum.RECEIVE)
                            .setStatus(
                                    SMessage.MessageBuf.Status.newBuilder()
                                    .setState(true)
                                    .setServerTime(System.currentTimeMillis())
                                    .setMessage("接收消息")
                                    .setSessionId(buf.getSessionId())
                            )
                            .setMessage(
                                    buf.getMessage()
                            ).build();
                    message.setCommand(KafkaCommand.MESSAGE);
                    message.setBody("接收消息");
                    message.setMessageTime(System.currentTimeMillis());
                    message.setMessageBuf(result);
                    sendMessageProducer.send(topic, message);
                });
            }
        }
    }

    /**
     * 处理接收到的信息处理
     * @param message
     */
    private void responseFromReceive(KafkaMessage message) {
        long messageId = message.getMessageBuf().getMessageId();
        String userId = message.getUserId();
        //找到未读消息中对应的这条未读消息
        List<UnRead> unReads = unReadRepository.getAll(userId);
        if (unReads != null) {
            unReads.forEach(unRead -> {
                if (unRead.getMessageBuf().getMessage().getMessageId() == messageId) {
                    unReadRepository.delete(userId, unRead);
                }
            });
        }
    }

}
package com.sinsz.im.configuration;

import com.sinsz.im.commonly.WorkThreadFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.*;

@Configuration
public class ExecutorConfiguration {

    @Value("${executor.corePoolSize:0}")
    private int corePoolSize;

    @Value("${executor.maximumPoolSize:0}")
    private int maximumPoolSize;

    @Value("${executor.keepAliveTime:0}")
    private int keepAliveTime;

    @Value("${executor.schCorePoolSize:0}")
    private int schCorePoolSize;

    @Bean
    public ExecutorService executorService() {
        return new ThreadPoolExecutor(
                corePoolSize,
                maximumPoolSize,
                keepAliveTime,
                TimeUnit.SECONDS,
                new LinkedBlockingQueue<>(),
                new WorkThreadFactory("EXE")
        );
    }

    @Bean
    public ScheduledExecutorService scheduledExecutorService() {
        return Executors.newScheduledThreadPool(
                schCorePoolSize,
                new WorkThreadFactory("SCH")
        );
    }

}

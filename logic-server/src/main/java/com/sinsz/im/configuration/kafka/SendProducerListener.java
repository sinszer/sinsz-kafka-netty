package com.sinsz.im.configuration.kafka;

import com.sinsz.im.common.SpringContextHolder;
import com.sinsz.im.event.KafkaEvent;
import com.sinsz.im.event.KafkaObject;
import com.sinsz.im.kafka.KafkaMessage;
import com.sinsz.im.repository.KafkaReceiveFailRepository;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.support.ProducerListener;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * kafka生产者监听
 * @author chenjianbo
 * @date 2017/9/26
 */
@Component
public class SendProducerListener implements ProducerListener<String, KafkaMessage> {

    private static final Logger logger = LoggerFactory.getLogger(SendProducerListener.class);

    @Autowired
    private KafkaReceiveFailRepository kafkaReceiveFailRepository;

    /**
     * 发送成功调用
     * @param topic
     * @param partition
     * @param key
     * @param value
     * @param recordMetadata
     */
    @Override
    public void onSuccess(String topic, Integer partition, String key,
                          KafkaMessage value,
                          RecordMetadata recordMetadata) {
        logger.info("发送成功...");
        String k = key + "_logic";
        if (kafkaReceiveFailRepository.exist(k)) {
            kafkaReceiveFailRepository.delete(k);
            logger.debug("清理成功！key:{}", k);
        }
    }

    /**
     * 发送失败调用,首先处理重发机制3次重发，3次结束仍然失败则丢弃
     * @param topic
     * @param partition
     * @param key
     * @param value
     * @param exception
     */
    @Override
    public void onError(String topic, Integer partition, String key,
                        KafkaMessage value, Exception exception) {
        String k = key + "_logic";
        int retryNum = kafkaReceiveFailRepository.get(k);
        System.out.println("发送失败" + retryNum + "次");
        if (retryNum < 3) {
            //开启监听重发
            KafkaObject object = new KafkaObject(topic, partition, key, value);
            SpringContextHolder.getApplicationContext().publishEvent(
                    new KafkaEvent<>(object)
            );
        } else {
            //开启推送
            switch (value.getMessageBuf().getRequest()) {
                case RESPSENDINFO:
                case NOTICE:
                case RECEIVE:

                    // TODO: 2017/10/17 推送到客户端


                    break;
                default:
                    logger.error("不支持的推送类型。{}", new Date());
            }
            kafkaReceiveFailRepository.delete(k);
        }
    }

    /**
     * 需要验证成功或失败设置为true，false表示不感兴趣
     * @return
     */
    @Override
    public boolean isInterestedInSuccess() {
        return true;
    }
}

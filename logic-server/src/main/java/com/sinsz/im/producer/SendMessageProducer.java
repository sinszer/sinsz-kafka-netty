package com.sinsz.im.producer;

import com.sinsz.im.kafka.KafkaMessage;
import com.sinsz.im.kafka.KafkaSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

/**
 * 将处理好的消息内容回执给netty连接管理器
 * @author chenjianbo
 * @date 2017/9/27
 */
@Component
public class SendMessageProducer {

    @Autowired
    private KafkaTemplate<String, KafkaMessage> kafkaTemplate;

    /**
     * 发送消息到队列
     * @param topic
     * @param msg
     */
    public void send(String topic, KafkaMessage msg) {
        KafkaSender.getInstance().defaultSend(
                kafkaTemplate,
                topic,
                String.valueOf(System.nanoTime()),
                msg
        );
    }

    /**
     * 重发
     * @param topic
     * @param partition
     * @param key
     * @param value
     */
    public void listenerRetry(
            String topic,
            Integer partition,
            String key,
            KafkaMessage value){
        KafkaSender.getInstance().sender(
                kafkaTemplate,
                topic,
                key,
                partition,
                value
        );
    }

}
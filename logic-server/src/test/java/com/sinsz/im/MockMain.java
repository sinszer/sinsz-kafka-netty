package com.sinsz.im;

import com.sinsz.im.enums.AppEnum;
import org.nutz.http.Http;
import org.nutz.http.Response;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * 模拟用户相关操作数据
 * @author chenjianbo
 * @date 2017/10/11
 */
public class MockMain {

    private MockMain() {
    }

    private static void reg(String user, String pwd, String name) {
        Map<String, Object> params = new HashMap<>();
        params.put("user",      user);
        params.put("passwd",    pwd);
        params.put("name",      name);
        commonly("http://localhost:7778/mock/reg", params);
    }

    private static void login(String user, String pwd, AppEnum app, String deviceToken) {
        Map<String, Object> params = new HashMap<>();
        params.put("user",          user);
        params.put("passwd",        pwd);
        params.put("app",           app);
        params.put("deviceToken",   deviceToken);
        commonly("http://localhost:7778/mock/login", params);
    }

    private static void commonly(String url, Map<String, Object> params) {
        Response response = Http.get(url, params, 5000);
        if (response.isOK()) {
            System.out.println(response.getContent());
        } else {
            System.out.println("请求失败");
        }
    }

    private static AppEnum app() {
        AppEnum[] enums = AppEnum.values();
        int num = new Random().nextInt(enums.length);
        return enums[num];
    }

    /**
     * admin -> 111111
     * admin2 -> 111111
     * @param args
     */
    public static void main(String[] args) {
        login("admin2", "111111", app(), "");
    }

}